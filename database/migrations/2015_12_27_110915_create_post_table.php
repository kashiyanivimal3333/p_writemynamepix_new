<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post', function(Blueprint $table) {
            $table->increments('id');
            $table->integer('subcategory_id');
            $table->string('slug');
            $table->string('title');
            $table->string('image_sample');
            $table->string('image_blank');
            $table->string('enter_your_name');
            $table->integer('char_limit');
            $table->integer('x_coordinate');
            $table->integer('y_coordinate');
            $table->integer('rotate');
            $table->integer('font_size');
            $table->integer('font_type');
            $table->string('font_color');
            $table->text('description');
            $table->integer('download');
            $table->integer('view');
            $table->text('meta_keyword');
            $table->text('meta_description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
