<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Config;
use DB;

class Post extends Model
{
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'post';

    protected $guarded = array();

    public function getPosts($input)
    {
        $data = static::select("post.*")->orderBy('id', 'DESC');

        if (!empty($input['filter']) && is_array($input['filter'])) {
            foreach ($input['filter'] as $column => $row) {
                if (!empty($column) && !empty($row["value"]) && is_array($row)) {
                    $operator = Config::get("setting.type", 1)[$row["type"]];
                    if ($row["type"] == 7) {
                        $data->where($column, $operator, "%{$row["value"]}%");
                    } else {
                        $data->where($column, $operator, $row["value"]);
                    }
                }
            }
        }

        return $data = $data->paginate(10);
    }

    public function AddPost($input)
    {
        return static::create($input);
    }

    public function editPost($input)
    {
        return static::where('id',$input['id'])->update(array_except($input,array('id')));
    }

    public function removePost($id)
    {
        return static::where('id',$id)->delete();
    }

    public function getTotalPost()
    {
        return static::count();
    }

    public function getFrontFeaturedPost()
    {
        return static::select('post.*','sub_categories.name as subCategoryName')
                ->join('sub_categories','sub_categories.id','=','post.subcategory_id')
                ->orderBy('view','desc')
                ->take(6)
                ->get();
    }

    public function getFrontLatestPost()
    {
        return static::select('post.*','categories.name as categoriesName'
                ,'sub_categories.name as subCategoryName'
                ,'sub_categories.slug as subCategorySlug'
                )
                ->join('sub_categories','sub_categories.id','=','post.subcategory_id')
                ->join('categories','categories.id','=','sub_categories.category_id')
                ->where("post.status",1)
                ->orderBy("post.status_date","desc")
                ->paginate(6);
    }

    public function getPostWithCategory($slug)
    {
        return static::select('post.*'
                ,'categories.name as categoriesName'
                ,'sub_categories.name as subCategoryName'
                ,'sub_categories.slug as subCategorySlug'
                )
                ->join('sub_categories','sub_categories.id','=','post.subcategory_id')
                ->join('categories','categories.id','=','sub_categories.category_id')
                ->where('sub_categories.slug',$slug)
                ->where("post.status",1)
                ->orderBy("post.status_date","desc")
                ->paginate(6);   
        
    }

    public function getPostWithSlug($slug)
    {
        return static::select('post.*','sub_categories.name as subCategoryName')
                ->join('sub_categories','sub_categories.id','=','post.subcategory_id')
                ->where('post.slug',$slug)
                ->first();
    }

    public function getPostWithSearch($search)
    {
        return static::select('post.*','sub_categories.name as subCategoryName')
                ->join('sub_categories','sub_categories.id','=','post.subcategory_id')
                ->where('post.title','like','%'.$search.'%')
                ->get(); 

    }

    public function getPostWithId($id)
    {
        return static::select('post.*','sub_categories.name as subCategoryName','fonts.font_file as fontFileName','fonts_se.font_file as fontFileName2')
                ->join('sub_categories','sub_categories.id','=','post.subcategory_id')
                ->join('fonts','fonts.id','=','post.font_type')
                ->leftjoin('fonts as fonts_se','fonts_se.id','=','post.font_type2')
                ->where('post.id',$id)
                ->first();   
    }

    public function getRelatedPost($id,$rel)
    {
        return static::select('post.*'
                ,'categories.name as categoriesName'
                ,'sub_categories.name as subCategoryName'
                ,'sub_categories.slug as subCategorySlug'
                )
                ->join('sub_categories','sub_categories.id','=','post.subcategory_id')
                ->join('categories','categories.id','=','sub_categories.category_id')
                ->where('sub_categories.id',$id)
                ->where('post.id','!=',$rel)
                ->orderBy('view','desc')
                ->take(3)
                ->get();
    }

    public function getFrontPopularPost()
    {
        return static::select('post.*'
                ,'categories.name as categoriesName'
                ,'sub_categories.name as subCategoryName'
                ,'sub_categories.slug as subCategorySlug'
                )
                ->join('sub_categories','sub_categories.id','=','post.subcategory_id')
                ->join('categories','categories.id','=','sub_categories.category_id')
                ->orderBy('download','desc')
                ->take(6)
                ->get();
    }

    public function getMaxSitemap(){
        return static::select('post.*')
                ->max('sitemap');
    }

    public function editKeywordPost($input){
        return static::where('id',$input['id'])->update(array_except($input,array('id')));
    }

    public function getPostWithIdName($slug)
    {
        return static::select('post.*','sub_categories.name as subCategoryName','fonts.font_file as fontFileName')
                ->join('sub_categories','sub_categories.id','=','post.subcategory_id')
                ->join('fonts','fonts.id','=','post.font_type')
                ->where('post.slug',$slug)
                ->first();   
    }

    public function postSelectAllName($input){
        return static::where('id',$input['id'])->update(array('is_all'=>$input['is_all']));
    }

    public function randomPost()
    {
        return static::select('post.*','categories.name as categoriesName','sub_categories.name as subCategoryName')
                ->join('sub_categories','sub_categories.id','=','post.subcategory_id')
                ->join('categories','categories.id','=','sub_categories.category_id')
                ->orderBy(DB::raw("RAND()"))
                ->take(6)
                ->get();
    }

    public function getPostNameDataRandom()
    {
        return static::select('post.*'
                ,'categories.name as categoriesName'
                ,'sub_categories.name as subCategoryName'
                ,'sub_categories.slug as subCategorySlug'
                )
                ->join('sub_categories','sub_categories.id','=','post.subcategory_id')
                ->join('categories','categories.id','=','sub_categories.category_id')
		        ->where('post.subcategory_id',22)
                ->where('is_alpha',0)
                ->orderBy(DB::raw("RAND()"))
                ->first();
    }

    public function changePostStatus($input)
    {
        return static::where("id",$input['id'])->update(['title'=>$input['title'],'slug'=>$input['slug'],'status'=>$input['status'],'status_date'=>date('Y-m-d H:i:s')]);
    }

}
