<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Config;
use DB;

class Name extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'names';

    protected $guarded = array();

    public function getNames($input){
        $data = static::select("names.*");

        if (!empty($input['filter']) && is_array($input['filter'])) {
            foreach ($input['filter'] as $column => $row) {
                if (!empty($column) && !empty($row["value"]) && is_array($row)) {
                    $operator = Config::get("setting.type", 1)[$row["type"]];
                    if ($row["type"] == 7) {
                        $data->where($column, $operator, "%{$row["value"]}%");
                    } else {
                        $data->where($column, $operator, $row["value"]);
                    }
                }
            }
        }

        return $data = $data->paginate(10);
    }

    public function AddName($input){
        return static::create($input);
    }

    public function getPostNameData($name){
        return static::where('name',$name)->first();
    }

    public function getNameAll(){
        return static::select("names.*")->lists('name','id');;
    }
}
