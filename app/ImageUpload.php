<?php 
namespace App;

use DB;
use File;

class ImageUpload
{
    public static function upload($path, $image)
    {
        $a=rand(123456789,2);
        $imageName = time().'_'.$a.'.'.$image->getClientOriginalExtension();
        $image->move($path,$imageName);
        return $imageName;
    }

    public static function removeImage($path, $id, $table, $imag = 'imagepath')
    {
        $check = DB::table($table)->where('id',$id)->first();
        if(!is_null($check)){
            if($imag == 'path'){
                $path = public_path($path.'/'.$check->path);
            }else{
                $path = public_path($path.'/'.$check->imagepath);
            }
            if(File::exists($path)){
                File::delete($path);
            }
        }
    }
}
