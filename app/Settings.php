<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Config;
use DB;

class Settings extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'settings';

    protected $guarded = array();

    public function getSettings()
    {
        $data = Settings::get()->toArray();
        $result = [];
        foreach ($data as $key => $value) {
            $result[$value['slug']] = $value;
        }
        return $result;
    }

    public function updateSettings($input)
    {
        foreach ($input as $key=>$value){
           Settings::where('slug',$key)->update(array('value'=>$value));  
        }
        return;
    }
}
