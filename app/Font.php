<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Config;
use DB;

class Font extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'fonts';

    protected $guarded = array();

    public function getFont($input)
    {
        $data = static::select("fonts.*");

        if (!empty($input['filter']) && is_array($input['filter'])) {
            foreach ($input['filter'] as $column => $row) {
                if (!empty($column) && !empty($row["value"]) && is_array($row)) {
                    $operator = Config::get("setting.type", 1)[$row["type"]];
                    if ($row["type"] == 7) {
                        $data->where($column, $operator, "%{$row["value"]}%");
                    } else {
                        $data->where($column, $operator, $row["value"]);
                    }
                }
            }
        }

        return $data = $data->paginate(10);
    }

    public function AddFont($input)
    {
        return static::create($input);
    }

    public function removeFont($id)
    {
        return static::where('id',$id)->delete();
    }

    public function checkFontFile($fileName)
    {
        return static::where('font_file',$fileName)->first();
    }

    public function getFontFileList($value='')
    {
        return DB::table('fonts')->lists('font_file','id');
    }
}
