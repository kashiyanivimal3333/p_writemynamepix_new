<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Config;
use DB;

class Categories extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'categories';

    protected $guarded = array();

    public function getCategories($input)
    {
        $data = static::select("categories.*");

        if (!empty($input['filter']) && is_array($input['filter'])) {
            foreach ($input['filter'] as $column => $row) {
                if (!empty($column) && !empty($row["value"]) && is_array($row)) {
                    $operator = Config::get("setting.type", 1)[$row["type"]];
                    if ($row["type"] == 7) {
                        $data->where($column, $operator, "%{$row["value"]}%");
                    } else {
                        $data->where($column, $operator, $row["value"]);
                    }
                }
            }
        }

        return $data = $data->paginate(10);
    }

    public function AddCategory($input)
    {
        return static::create($input);
    }

    public function editCategory($input)
    {
        return static::where('id',$input['id'])->update(array_except($input,array('id')));
    }

    public function removeCategory($id)
    {
        return static::where('id',$id)->delete();
    }

    public function getCategoryList()
    {
        return DB::table('categories')->lists('name','id');
    }

    public function getFrontCategories()
    {
        $data = static::select('categories.*','sub_categories.id as subCategoryId','sub_categories.name as subCategoryName','sub_categories.slug as subCategorySlug')
                ->leftjoin('sub_categories','sub_categories.category_id','=','categories.id')
                ->get();


        $result=array();
        if(!empty($data) && $data->count()){
            foreach ($data as $key => $value) {
                $result[$value->id]['id'] = $value->id;
                $result[$value->id]['name'] = $value->name;
                if (!empty($value->subCategoryId)) {
                    $result[$value->id]['subCatData'][$value->subCategoryId]['subCategoryName'] = $value->subCategoryName;
                    $result[$value->id]['subCatData'][$value->subCategoryId]['subCategorySlug'] = $value->subCategorySlug;
                }
            }
        } 
        
        // echo "<pre>";
        // print_r($result);
        // exit();
        return $result;
    }
}
