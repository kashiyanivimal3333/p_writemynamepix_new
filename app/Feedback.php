<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Config;
use DB;

class Feedback extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'feedback';

    protected $guarded = array();

    public function getFeedback($input)
    {
        return $data = static::paginate(10);
    }

    public function AddFeedback($input)
    {
        return static::create($input);
    }

    public function removeFeedback($id)
    {
        return static::where('id',$id)->delete();
    }

    public function getTotalFeedback()
    {
        return static::count();
    }
}
