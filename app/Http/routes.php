<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', array('as'=> 'front.home', 'uses' => 'FrontHomeController@index'));

Route::get('howtowrite.html', array('as'=> 'front.howtowrite', 'uses' => 'FrontHomeController@howtowrite'));
Route::get('about.html', array('as'=> 'front.about', 'uses' => 'FrontHomeController@about'));
Route::get('policy.html', array('as'=> 'front.policy', 'uses' => 'FrontHomeController@policy'));
Route::get('contact.html', array('as'=> 'front.contact', 'uses' => 'FrontHomeController@contact'));
Route::post('fedbackCreate', array('as'=> 'front.feedback.create', 'uses' => 'FrontHomeController@createFeedback'));

Route::post('search', array('as'=> 'front.search.get', 'uses' => 'FrontHomeController@SearchPost'));

Route::get('post/{name}/{id}/details.html', array('as'=> 'front.namedetails', 'uses' => 'FrontHomeController@postDetailsName'));
Route::get('downloadLinkName/{id}', array('as'=> 'front.downloadName.get', 'uses' => 'FrontHomeController@getDownloadLinkName'));

Route::get('download/{id}', array('as'=> 'front.printText.get', 'uses' => 'FrontHomeController@PrintText'));
Route::get('downloadLink/{id}', array('as'=> 'front.download.get', 'uses' => 'FrontHomeController@getDownloadLink'));

Route::get('admin/login', array('as'=> 'admin.login', 'uses' => 'Auth\AdminAuthController@login'));
Route::post('admin/login', array('as'=> 'admin.login.post', 'uses' => 'Auth\AdminAuthController@loginPost'));

Route::group(['prefix' => 'admin','middleware' => 'auth'], function () {
	Route::get('home', array('as'=> 'admin.home', 'uses' => 'HomeController@index'));

	Route::get('logout', array('as'=> 'admin.logout', 'uses' => 'Auth\AdminAuthController@logout'));

	// UserController
	Route::get('users', array('as'=> 'admin.users', 'uses' => 'UserController@index'));
	Route::post('userCreate', array('as'=> 'admin.users.create', 'uses' => 'UserController@create'));
	Route::post('userEdit', array('as'=> 'admin.users.edit', 'uses' => 'UserController@edit'));
	Route::post('userRemove', array('as'=> 'admin.users.remove', 'uses' => 'UserController@delete'));
	Route::get('userRevoke/{id}', array('as'=> 'admin.users.revoke', 'uses' => 'UserController@revoke'));
	Route::post('userBan', array('as'=> 'admin.users.ban', 'uses' => 'UserController@ban'));

	// CategiriesController
	Route::get('categories', array('as'=> 'admin.categories', 'uses' => 'CategiriesController@index'));
	Route::post('categoryCreate', array('as'=> 'admin.category.create', 'uses' => 'CategiriesController@create'));
	Route::post('categoryEdit', array('as'=> 'admin.category.edit', 'uses' => 'CategiriesController@edit'));
	Route::post('categoryRemove', array('as'=> 'admin.category.remove', 'uses' => 'CategiriesController@delete'));

	// SubCategiriesController
	Route::get('subCategories', array('as'=> 'admin.subCategories', 'uses' => 'SubCategiriesController@index'));
	Route::post('subCategoryCreate', array('as'=> 'admin.subCategory.create', 'uses' => 'SubCategiriesController@create'));
	Route::post('subCategoryEdit', array('as'=> 'admin.subCategory.edit', 'uses' => 'SubCategiriesController@edit'));
	Route::post('subCategoryRemove', array('as'=> 'admin.subCategory.remove', 'uses' => 'SubCategiriesController@delete'));

	// FontControllesr
	Route::get('fonts', array('as'=> 'admin.fonts', 'uses' => 'FontPostController@index'));
	Route::post('fontCreate', array('as'=> 'admin.font.create', 'uses' => 'FontPostController@create'));
	Route::post('fontRemove', array('as'=> 'admin.font.remove', 'uses' => 'FontPostController@delete'));

	// PostController
	Route::get('post', array('as'=> 'admin.post', 'uses' => 'PostController@index'));
	Route::post('postCreate', array('as'=> 'admin.post.create', 'uses' => 'PostController@create'));
	Route::post('postEdit', array('as'=> 'admin.post.edit', 'uses' => 'PostController@edit'));
	Route::post('postRemove', array('as'=> 'admin.post.remove', 'uses' => 'PostController@delete'));
	Route::get('addName', array('as'=> 'admin.post.addname', 'uses' => 'PostController@addName'));
	Route::post('createAddName', array('as'=> 'admin.post.create.addname', 'uses' => 'PostController@postAddName'));
	Route::post('postKeywordEdit', array('as'=> 'admin.postkeyword.edit', 'uses' => 'PostController@editKeyword'));
	Route::post('changePostStatus', array('as'=> 'admin.changePostStatus.update', 'uses' => 'PostController@changePostStatus'));

	// FeedbackController
	Route::get('feedback', array('as'=> 'admin.feedback', 'uses' => 'FeedbackController@index'));
	Route::post('feedbackRemove', array('as'=> 'admin.feedback.remove', 'uses' => 'FeedbackController@delete'));
	
	// SliderController
	Route::get('slider', array('as'=> 'admin.slider', 'uses' => 'SliderController@index'));
	Route::post('sliderCreate', array('as'=> 'admin.slider.create', 'uses' => 'SliderController@create'));
	Route::post('sliderRemove', array('as'=> 'admin.slider.remove', 'uses' => 'SliderController@delete'));

	// SettingsController
	Route::get('settings', array('as'=> 'admin.settings', 'uses' => 'SettingsController@index'));
	Route::post('settingsUpdate', array('as'=> 'admin.settings.update', 'uses' => 'SettingsController@create'));

	// NamesController
	Route::get('names', array('as'=> 'admin.names', 'uses' => 'NamesController@index'));
	Route::post('namesCreate', array('as'=> 'admin.name.create', 'uses' => 'NamesController@create'));

	Route::post('removeImages', array('as'=> 'admin.removeImages', 'uses' => 'HomeController@removeImages'));

	Route::get('generateNameImages', array('as'=> 'admin.generateNameImages', 'uses' => 'FrontHomeController@generateNameImages'));
});

Route::get('sitemap', function(){
	ini_set('memory_limit', '1024M');
	set_time_limit(0); //60 seconds = 1 minute

    // create new sitemap object
    $sitemap = App::make("sitemap");

    $sitemap->add(URL::to('/'), '2012-08-25T20:10:00+02:00', '1.0', 'daily');
    $sitemap->add(URL::to('/howtowrite.html'), '2016-02-09T20:10:00+02:00', '1.0', 'daily');
    $sitemap->add(URL::to('/about.html'), '2016-02-09T20:10:00+02:00', '1.0', 'daily');
    $sitemap->add(URL::to('/policy.html'), '2016-02-09T20:10:00+02:00', '1.0', 'daily');
    $sitemap->add(URL::to('/contact.html'), '2016-02-09T20:10:00+02:00', '1.0', 'daily');
    $sitemap->add(URL::to('/fedbackCreate.html'), '2016-02-09T20:10:00+02:00', '1.0', 'daily');

    $categorys = DB::table('sub_categories')->orderBy('created_at', 'desc')->get();

	// add every post to the sitemap
	foreach ($categorys as $category)
	{
	    $sitemap->add(URL::route('front.categoryDetail',$category->slug), $category->updated_at, '1.0', 'daily');
	}

	$posts = DB::table('post')
			->select("post.*","sub_categories.slug as subCategorySlug")
			->join("sub_categories","sub_categories.id","=","post.subcategory_id")
			->where("status",1)
			->orderBy('status_date', 'desc')
			->get();

    // add every post to the sitemap
    foreach ($posts as $post)
    {
     	$images = array();
         
            $images[] = array(
                'url' => URL::to('/')."/uploadImages/post/sample/".$post->image_sample,
                'title' => $post->title,
                'caption' => $post->title
            );
         
        $sitemap->add(URL::route('front.details',[$post->subCategorySlug,$post->slug]), $post->status_date, '1.0', 'daily', $images);
    }

    return $sitemap->render('xml');
});

Route::get('{id}', array('as'=> 'front.categoryDetail', 'uses' => 'FrontHomeController@listCategoryPost'));
Route::get('{category_slug}/{post_slug}', array('as'=> 'front.details', 'uses' => 'FrontHomeController@postDetails'));