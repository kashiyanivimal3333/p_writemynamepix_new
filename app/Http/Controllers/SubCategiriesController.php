<?php namespace App\Http\Controllers;

use Input;
use Validator;
use Response;
use Hash;
use Redirect;
use View;
use App\Categories;
use App\SubCategories;
use Request;

class SubCategiriesController extends AdminController
{
	
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
	public function __construct()
	{
		parent::__construct();
        $this->categories = new Categories;
        $this->subCategories = new SubCategories;
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
	public function index()
	{
        $input = Input::all();
        $displayData = $this->subCategories->getSubCategories($input);
        $categoryList= $this->categories->getCategoryList();
        $i = (Input::get('page', 1) - 1) * 10;

        if (Request::ajax()) {
            $respose['data'] = View::make('subCategories.data', array('displayData' => $displayData,'categoryList'=>$categoryList,'i'=>$i))->render();
            $respose['paginate'] = View::make('adminTheme.paginate', array('displayData' => $displayData))->render();
            return Response::json($respose);
        }

        return view('subCategories.index',compact('displayData','categoryList'))
            ->with('i', (Input::get('page', 1) - 1) * 5);

	}

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $input = Input::except(array('_token'));
        $validator = Validator::make($input, [
            'category_id' => 'required',
            'name' => 'required|unique:sub_categories,name',
            'title' => 'required',
        ]);

        if ($validator->passes()) {
            $input['slug'] = $this->url_slug($input['name']);
            $this->subCategories->AddSubCategory($input);
            return Response::json(array('success'=>'done'));
        }

        return Response::json(array('error'=>$validator->errors()->all()));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function edit()
    {
        $input = Input::except(array('_token'));

        $validator = Validator::make($input, [
            'category_id' => 'required',
            'name' => 'required|unique:sub_categories,name,'.$input['id'],
            'title' => 'required',
        ]);
        
        if(!isset($input['is_new'])){
            $input['is_new'] = 0;
            $input['new_name'] = '';
        }

        if ($validator->passes()) {
            $input['slug'] = $this->url_slug($input['name']);
            $this->subCategories->editSubCategory($input);
            return Response::json(array('success'=>'done'));
        }

        return Response::json(array('error'=>$validator->errors()->all()));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function delete()
    {
        $input = Input::all();
        $this->subCategories->removeSubCategory($input['id']);
        return Redirect::route('admin.subCategories')
                ->with('success','Sub Category deteted successfully.');
    }

}
