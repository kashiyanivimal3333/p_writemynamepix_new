<?php namespace App\Http\Controllers;

use Input;
use Validator;
use Response;
use Hash;
use Redirect;
use View;
use App\Settings;
use App\ImageUpload;

class SettingsController extends AdminController
{
	
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
	public function __construct()
	{
		parent::__construct();
		$this->settings = new Settings;
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
	public function index()
	{
        $input = Input::all();
		$settings = $this->settings->getSettings();

		return view('settings.index',compact('settings'));
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
	public function create()
	{ 
		$input = Input::except(array('_token'));
        // dd($input);

        $validatorGeneral = Validator::make($input, [
            
        ]);

        if ($validatorGeneral->passes()) {

            if(Input::hasFile('site-logo')){
                 $input['site-logo'] = ImageUpload::upload('uploadImages/settings',Input::file('site-logo'));
            }else{
                $input = array_except($input,array('site-logo')); 
            }

            if(Input::hasFile('site-favicon')){
                 $input['site-favicon'] = ImageUpload::upload('uploadImages/settings',Input::file('site-favicon'));
            }else{
                $input = array_except($input,array('site-favicon')); 
            }

            $this->settings->updateSettings($input);

            return Redirect::route('admin.settings')
                ->with('success','Settings Successfully Update.');
        }
        
        return Redirect::route('admin.settings')
                ->withInput()
                ->with('errors',$validator->errors());
	}

}
