<?php namespace App\Http\Controllers;

use Input;
use App\User;
use Validator;
use Response;
use Hash;
use Request;
use View;
use Redirect;

class UserController extends AdminController
{
    

	protected $user;
	
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
	public function __construct()
	{
		parent::__construct();
		$this->user = new User;
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
	public function index()
	{
        $input = Input::all();
		$users = $this->user->getUesrs($input);
        $i = (Input::get('page', 1) - 1) * 10;

        if (Request::ajax()) {
            $respose['data'] = View::make('users.data', array('users' => $users,'i'=>$i))->render();
            $respose['paginate'] = View::make('adminTheme.paginate', array('users' => $users))->render();
            return Response::json($respose);
        }

		return view('users.index',compact('users'))
            ->with('i', (Input::get('page', 1) - 1) * 5);
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
	public function create()
	{
		$input = Input::except(array('_token'));
        $validator = Validator::make($input, [
            'email' => 'required|email|unique:users,email',
            'password' => 'required|same:password_confirmation',
            'name' => 'required',
        ]);

        if ($validator->passes()) {
            $input['password'] = Hash::make($input['password']);
            $input = array_except($input,array('password_confirmation'));
            $this->user->AddUser($input);
            return Response::json(array('success'=>'done'));
        }

        return Response::json(array('error'=>$validator->errors()->all()));
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
	public function edit()
	{
		$input = Input::except(array('_token'));
        $validator = Validator::make($input, [
            'email' => 'required|email|unique:users,email,'.$input['id'],
            'password' => 'same:password_confirmation',
            'name' => 'required',
        ]);

        if ($validator->passes()) {
        	if(!empty($input['password'])){	
            	$input['password'] = Hash::make($input['password']);
        	}else{
        		$input = array_except($input,array('password'));	
        	}

            $input = array_except($input,array('password_confirmation'));

            $this->user->editUser($input);
            return Response::json(array('success'=>'done'));
        }

        return Response::json(array('error'=>$validator->errors()->all()));
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
	public function delete()
    {
        $input = Input::all();
        $this->user->removeUser($input['id']);
        return Redirect::route('admin.users')
                ->with('success','User deteted successfully.');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function ban()
    {
        $input = Input::all();
        if(!empty($input['id'])){
            $this->user->userBan($input);
        }
        return Redirect::route('admin.users')
            ->with('success','User Ban successfully.');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function revoke($id)
    {
        if(!empty($id)){
            $this->user->userRevoke($id);
        }
        return Redirect::route('admin.users')
            ->with('success','User Revoke successfully.');
    }

}
