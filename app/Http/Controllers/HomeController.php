<?php namespace App\Http\Controllers;

use Input;
use App\SubCategories;
use App\Post;
use App\Slider;
use App\Feedback;
use File;

class HomeController extends AdminController
{
	
	public function __construct()
	{
		parent::__construct();
		$this->subCategory = new SubCategories;
		$this->post = new Post;
		$this->slider = new Slider;
		$this->feedback = new Feedback;
	}

	public function index()
	{
		$totalSubCategory = $this->subCategory->getTotalSubCategory();
		$totalPost = $this->post->getTotalPost();
		$totalSLider = $this->slider->getTotalSliderImage();
		$totalFeedback = $this->feedback->getTotalFeedback();
		$countImages = $this->countImages();

		return view('home.admin',compact('countImages','totalSubCategory','totalPost','totalSLider','totalFeedback'));
	}

	public function countImages()
	{
		$files = File::files(public_path('textPrintImage'));
		$filecount = 0;

		if ( $files !== false ){
		    $filecount = count( $files );
		}

		return $filecount;
	}

	public function removeImages()
	{
		File::cleanDirectory(public_path('textPrintImage'));
		return back();
	}

}
