<?php namespace App\Http\Controllers;

use Input;
use Auth;
use Request;
use Config;
use View;
use Session;

class AdminController extends Controller
{
	
	public function __construct()
	{
		View::share('theme','adminTheme.default');
		View::share('login','adminTheme.login');
		View::share('projectTitle','Admin Login');
	}

	public function setAdminConfig()
	{
		Config::set('auth.model',\App\Admin::class);
        $authSetModel = Auth::createEloquentDriver();
        Auth::setProvider($authSetModel->getProvider());
	}

	public function setUserConfig()
	{
		Config::set('auth.model',\App\User::class);
        $authSetModel = Auth::createEloquentDriver();
        Auth::setProvider($authSetModel->getProvider());
	}

	function url_slug($str, $replace=array(), $delimiter='-', $maxLength=200) {

		if( !empty($replace) ) {
			$str = str_replace((array)$replace, ' ', $str);
		}

		$clean = iconv('UTF-8', 'ASCII//TRANSLIT', $str);
		$clean = preg_replace("%[^-/+|\w ]%", '', $clean);
		$clean = strtolower(trim(substr($clean, 0, $maxLength), '-'));
		$clean = preg_replace("/[\/_|+ -]+/", $delimiter, $clean);

		return $clean;
	}
}
