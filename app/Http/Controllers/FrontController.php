<?php namespace App\Http\Controllers;

use View;
use App\Post;
use App\Categories;
use App\SubCategories;
use App\Settings;
use URL;
class FrontController extends Controller
{
	public $siteURL;

	public function __construct()
	{
		$this->siteURL = URL::to('/');
		$this->categories = new Categories;
		$this->subCategories = new SubCategories;
		$this->post = new Post;

		$categoryNew = $this->subCategories->getFrontCategoriesNew();

		$categoryList = $this->categories->getFrontCategories();
		$featuredPostList = $this->post->getFrontFeaturedPost();
		$randomPost = $this->post->randomPost();

		$settings = Settings::get()->lists('value', 'slug');

		View::share('FrontTheme','frontTheme.default');
		View::share('FrontThemeNew','frontThemeNew.default');

		View::share('categoryNew',$categoryNew);
		View::share('categoryList',$categoryList);
		View::share('featuredPostList',$featuredPostList);
		View::share('randomPost',$randomPost);
		
		View::share('settings',$settings);
	}
}
