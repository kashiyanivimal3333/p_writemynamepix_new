<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use App\Http\Controllers\AdminController;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Input;
use Illuminate\Contracts\Auth\Guard;
use Redirect;
use Session;
use Auth;

class AdminAuthController extends AdminController
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;
    protected $auth;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct(Guard $auth)
    {
        parent::__construct();
        $this->auth = $auth;
        // $this->setAdminConfig();
    }

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function login()
    {
        return view('auth.adminLogin');
    }

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function loginPost()
    {
        $input = Input::all();
        $validator = Validator::make($input, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if ($validator->passes()) {
            if ($this->auth->attempt(array('email' => $input['email'], 'password' => $input['password'])))
            {
                // Session::put('login_admin', Auth::user());
                return Redirect::route('admin.home');
            }else{
                return Redirect::route('admin.login')
                    ->with('error','your username and password are wrong.');
            }
        }

        return Redirect::route('admin.login')
            ->with('errors',$validator->errors());
    }

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function logout()
    {
        Auth::logout();
        Session::flush();
        return Redirect::to('/');
    }
}
