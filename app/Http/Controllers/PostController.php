<?php namespace App\Http\Controllers;

use Input;
use Validator;
use Response;
use Hash;
use Redirect;
use View;
use App\Post;
use App\Name;
use App\PostName;
use App\SubCategories;
use App\Font;
use Request;
use App\ImageUpload;

class PostController extends AdminController
{
	
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
	public function __construct()
	{
		parent::__construct();
        $this->post = new Post;
        $this->subCategory = new SubCategories;
        $this->font = new Font;
        $this->name = new Name;
		$this->postName = new PostName;
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
	public function index()
	{
        $input = Input::all();
        $displayData = $this->post->getPosts($input);
        $subCategoryList = $this->subCategory->getSubCategoryList();
        $fontFileList = $this->font->getFontFileList();
        $i = (Input::get('page', 1) - 1) * 10;

        if (Request::ajax()) {
            $respose['data'] = View::make('post.data', array('displayData' => $displayData,'subCategoryList'=>$subCategoryList,'fontFileList'=>$fontFileList,'i'=>$i))->render();
            $respose['paginate'] = View::make('adminTheme.paginate', array('displayData' => $displayData))->render();
            return Response::json($respose);
        }

        return view('post.index',compact('displayData','subCategoryList','fontFileList'))
            ->with('i', (Input::get('page', 1) - 1) * 5);

	}

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $input = Input::except(array('_token'));
        $validator = Validator::make($input, [
            'subcategory_id' => 'required',
            'title' => 'required',
            'image_sample' => 'required',
            'image_blank' => 'required',
            'enter_your_name' => 'required',
            'char_limit' => 'required|numeric',
            'rotate' => 'required|numeric',
            'coordinate'=>'required',
            'x_coordinate' => 'required|numeric',
            'y_coordinate' => 'required|numeric',
            'x2_coordinate' => 'required_if:coordinate,2|numeric',
            'y2_coordinate' => 'required_if:coordinate,2|numeric',
            'font_size' => 'required|numeric',
            'font_color' => 'required',
            'font_type' => 'required|numeric',
            'font_size2' => 'required_if:coordinate,2|numeric',
            'font_color2' => 'required_if:coordinate,2',
            'font_type2' => 'required_if:coordinate,2|numeric',
            'description' => 'required',
            'meta_keyword' => 'required',
            'meta_description' => 'required',
        ]);

        if ($validator->passes()) {
            $input['slug'] = $this->url_slug($input['title']);

            $input['image_sample'] = ImageUpload::upload('uploadImages/post/sample',Input::file('image_sample'));
            $input['image_blank'] = ImageUpload::upload('uploadImages/post/blank',Input::file('image_blank'));

            $this->post->AddPost($input);
            return Response::json(array('success'=>'done'));
        }

        return Response::json(array('error'=>$validator->errors()->all()));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function edit()
    {
        $input = Input::except(array('_token'));
        $validator = Validator::make($input, [
            'subcategory_id' => 'required',
            'title' => 'required',
            'enter_your_name' => 'required',
            'char_limit' => 'required|numeric',
            'rotate' => 'required|numeric',
            'coordinate'=>'required',
            'x_coordinate' => 'required|numeric',
            'y_coordinate' => 'required|numeric',
            'x2_coordinate' => 'required_if:coordinate,2|numeric',
            'y2_coordinate' => 'required_if:coordinate,2|numeric',
            'font_size' => 'required|numeric',
            'font_color' => 'required',
            'font_type' => 'required|numeric',
            'font_size2' => 'required_if:coordinate,2|numeric',
            'font_color2' => 'required_if:coordinate,2',
            'font_type2' => 'required_if:coordinate,2|numeric',
            'description' => 'required',
            'meta_keyword' => 'required',
            'meta_description' => 'required',
        ]);

        if ($validator->passes()) {

            if($input['coordinate'] == '1'){
                $input['x2_coordinate'] = '0';
                $input['y2_coordinate'] = '0';
            }

            if(Input::hasFile('image_sample')){
                $input['image_sample'] = ImageUpload::upload('uploadImages/post/sample',Input::file('image_sample'));
            }else{
                $input = array_except($input,array('image_sample')); 
            }

            if(Input::hasFile('image_blank')){
                $input['image_blank'] = ImageUpload::upload('uploadImages/post/blank',Input::file('image_blank'));
            }else{
                $input = array_except($input,array('image_blank')); 
            }

            $input['slug'] = $this->url_slug($input['title']);
            $this->post->editPost($input);
            return Response::json(array('success'=>'done'));
        }

        return Response::json(array('error'=>$validator->errors()->all()));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function delete()
    {
        $input = Input::all();
        $this->post->removePost($input['id']);
        return Redirect::route('admin.post')
                ->with('success','Post deteted successfully.');
    }

    public function addName(){
        $id = Input::get('id');
        $is_all = Input::get('is_all');
        $getName = $this->name->getNameAll();
        $getAddName = $this->postName->getAddName($id);
        // echo "<pre>";print_r($getAddName);exit;
        return view('post.addName',compact('id','is_all','getName','getAddName'));
    }

    public function postAddName(){
        $input = Input::get();
        if(!isset($input['is_all'])){
            $input['is_all'] = 0;
        }else{
            $input['is_all'] = 1;
        }

        if($input['is_all'] == 1){
            $this->post->postSelectAllName($input);
        }else{
            $data = $this->postName->postAddNames($input['id'], $input['name_id']);
        }

        return Redirect::route('admin.post');
    }

    public function editKeyword(){
        $input = Input::except(array('_token'));

        // $validator = Validator::make($input, [
        //     'name_keywords' => 'required',
        //     'name_description' => 'required',
        // ]);

        // if ($validator->passes()) {
            if($input['sitemap'] == 0){
                $max = $this->post->getMaxSitemap();
                $input['sitemap'] = $max+1;
            }
            $this->post->editKeywordPost($input);
            return Response::json(array('success'=>'done'));   
        // }

        return Response::json(array('error'=>$validator->errors()->all()));
    }

    public function changePostStatus($value='')
    {
        $input = Input::except(array('_token'));

        $validator = Validator::make($input, [
            'id' => 'required',
            'status' => 'required',
            'title' => 'required',
        ]);

        if ($validator->passes()) {
            $input['slug'] = $this->url_slug($input['title']);
            $this->post->changePostStatus($input);
            return Response::json(array('success'=>'done'));   
        }

        return Response::json(array('error'=>$validator->errors()->all()));
    }

}
