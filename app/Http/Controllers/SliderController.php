<?php namespace App\Http\Controllers;

use Input;
use Validator;
use Response;
use Hash;
use Redirect;
use View;
use App\Slider;
use Request;
use App\ImageUpload;

class SliderController extends AdminController
{
	
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
	public function __construct()
	{
		parent::__construct();
		$this->slider = new Slider;
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
	public function index()
	{
        $input = Input::all();
        $displayData = $this->slider->getSlider($input);
        $i = (Input::get('page', 1) - 1) * 10;

        if (Request::ajax()) {
            $respose['data'] = View::make('slider.data', array('displayData' => $displayData,'i'=>$i))->render();
            $respose['paginate'] = View::make('adminTheme.paginate', array('displayData' => $displayData))->render();
            return Response::json($respose);
        }

        return view('slider.index',compact('displayData'))
            ->with('i', (Input::get('page', 1) - 1) * 5);

	}

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $input = Input::except(array('_token'));
        $validator = Validator::make($input, [
            'image' => 'required|image',
        ]);

        if ($validator->passes()) {
            $input['image'] = ImageUpload::upload('uploadImages/slider',Input::file('image'));
            $this->slider->AddSlider($input);
            return Response::json(array('success'=>'done'));
        }

        return Response::json(array('error'=>$validator->errors()->all()));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function delete()
    {
        $input = Input::all();
        $this->slider->removeSlider($input['id']);
        return Redirect::route('admin.slider')
                ->with('success','Slider Image deteted successfully.');
    }

}
