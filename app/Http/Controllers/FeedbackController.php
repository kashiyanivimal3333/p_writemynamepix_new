<?php namespace App\Http\Controllers;

use Input;
use Validator;
use Response;
use Hash;
use Redirect;
use View;
use App\Feedback;
use Request;

class FeedbackController extends AdminController
{
	
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
	public function __construct()
	{
		parent::__construct();
		$this->feedback = new Feedback;
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
	public function index()
	{
        $input = Input::all();
        $displayData = $this->feedback->getFeedback($input);
        
        return view('feedback.index',compact('displayData'));

	}

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function delete()
    {
        $input = Input::all();
        $this->feedback->removeFeedback($input['id']);
        return Redirect::route('admin.feedback')
                ->with('success','Feedback deteted successfully.');
    }

}
