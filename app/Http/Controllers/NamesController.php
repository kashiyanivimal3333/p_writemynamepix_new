<?php namespace App\Http\Controllers;

use Input;
use Validator;
use Response;
use Hash;
use Redirect;
use View;
use App\Name;
use Request;

class NamesController extends AdminController
{
	
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
	public function __construct()
	{
		parent::__construct();
		$this->name = new Name;
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
	public function index()
	{
		$input = Input::all();
        $displayData = $this->name->getNames($input);
        $i = (Input::get('page', 1) - 1) * 10;

        if (Request::ajax()) {
            $respose['data'] = View::make('name.data', array('displayData' => $displayData,'i'=>$i))->render();
            $respose['paginate'] = View::make('adminTheme.paginate', array('displayData' => $displayData))->render();
            return Response::json($respose);
        }

        return view('name.index',compact('displayData'))
            ->with('i', (Input::get('page', 1) - 1) * 5);
	}

	public function create(){
		$input = Input::except(array('_token'));
        $validator = Validator::make($input, [
            'name' => 'required',
        ]);

        if ($validator->passes()) {
            $this->name->AddName($input);
            return Response::json(array('success'=>'done'));
        }

        return Response::json(array('error'=>$validator->errors()->all()));
	}

}
