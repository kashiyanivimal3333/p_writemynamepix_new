<?php namespace App\Http\Controllers;

use Input;
use Validator;
use Response;
use Hash;
use Redirect;
use View;
use App\Font;
use Request;

class FontPostController extends AdminController
{
	
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
	public function __construct()
	{
		parent::__construct();
		$this->font = new Font;
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
	public function index()
	{
        $input = Input::all();
        $displayData = $this->font->getFont($input);
        $i = (Input::get('page', 1) - 1) * 10;

        if (Request::ajax()) {
            $respose['data'] = View::make('fonts.data', array('displayData' => $displayData,'i'=>$i))->render();
            $respose['paginate'] = View::make('adminTheme.paginate', array('displayData' => $displayData))->render();
            return Response::json($respose);
        }

        return view('fonts.index',compact('displayData'))
            ->with('i', (Input::get('page', 1) - 1) * 5);

	}

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $input = Input::except(array('_token'));
        $validator = Validator::make($input, [
            'font_file' => 'required',
        ]);

        if ($validator->passes()) {
            $fileName = Input::file('font_file');
            $fileOriginalName = $fileName->getClientOriginalName();
            
            if(is_null($this->font->checkFontFile($fileOriginalName))){
                $path = 'uploadFontFile';
                $fileName->move($path,$fileOriginalName);
                $this->font->AddFont(array('font_file'=>$fileOriginalName));
                return Response::json(array('success'=>'done'));
            }else{
                return Response::json(array('error'=>array('This File Aleready Uploaded')));
            }
        }

        return Response::json(array('error'=>$validator->errors()->all()));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function delete()
    {
        $input = Input::all();
        $this->font->removeFont($input['id']);
        return Redirect::route('admin.fonts')
                ->with('success','Font File deteted successfully.');
    }

}
