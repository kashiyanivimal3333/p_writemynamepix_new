<?php namespace App\Http\Controllers;

use Input;
use Validator;
use Response;
use Hash;
use Redirect;
use View;
use App\Categories;
use Request;

class CategiriesController extends AdminController
{
	
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
	public function __construct()
	{
		parent::__construct();
		$this->categories = new Categories;
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
	public function index()
	{
        $input = Input::all();
        $displayData = $this->categories->getCategories($input);
        $i = (Input::get('page', 1) - 1) * 10;

        if (Request::ajax()) {
            $respose['data'] = View::make('categories.data', array('displayData' => $displayData,'i'=>$i))->render();
            $respose['paginate'] = View::make('adminTheme.paginate', array('displayData' => $displayData))->render();
            return Response::json($respose);
        }

        return view('categories.index',compact('displayData'))
            ->with('i', (Input::get('page', 1) - 1) * 5);

	}

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $input = Input::except(array('_token'));
        $validator = Validator::make($input, [
            'name' => 'required|unique:categories,name',
            'title' => 'required',
        ]);

        if ($validator->passes()) {
            $input['slug'] = $this->url_slug($input['name']);
            $this->categories->AddCategory($input);
            return Response::json(array('success'=>'done'));
        }

        return Response::json(array('error'=>$validator->errors()->all()));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function edit()
    {
        $input = Input::except(array('_token'));
        $validator = Validator::make($input, [
            'name' => 'required|unique:categories,name,'.$input['id'],
            'title' => 'required',
        ]);

        if ($validator->passes()) {
            $input['slug'] = $this->url_slug($input['name']);
            $this->categories->editCategory($input);
            return Response::json(array('success'=>'done'));
        }

        return Response::json(array('error'=>$validator->errors()->all()));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function delete()
    {
        $input = Input::all();
        $this->categories->removeCategory($input['id']);
        return Redirect::route('admin.categories')
                ->with('success','Category deteted successfully.');
    }

}
