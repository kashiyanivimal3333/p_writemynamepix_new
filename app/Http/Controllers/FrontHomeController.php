<?php namespace App\Http\Controllers;

use View;
use Input;
use Validator;
use Redirect;
use App\Slider;
use App\Categories;
use App\SubCategories;
use App\Feedback;
use App\Post;
use App\Name;
use App\Settings;
use Intervention\Image\ImageManagerStatic as Image;
use Response;
use SEOMeta;
use OpenGraph;
use Twitter;
use SEO;
use URL;
use DB;
use File;

class FrontHomeController extends FrontController
{

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function __construct()
    {
        parent::__construct();
        $this->slider = new Slider;
        $this->categories = new Categories;
        $this->subCategories = new SubCategories;
        $this->feedback = new Feedback;
        $this->post = new Post;
        $this->name = new Name;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */

    public function SEOData()
    {
        $settings = Settings::get()->lists('value', 'slug');
        SEOMeta::setTitle($settings['site-title']);
        SEOMeta::setDescription($settings['site-description']);
        SEOMeta::addKeyword($settings['site-keyword']);

        OpenGraph::setDescription($settings['site-description']);
        OpenGraph::setTitle($settings['site-title']);
        OpenGraph::setUrl($this->siteURL);
        OpenGraph::addProperty('type', 'articles');

        Twitter::setTitle($settings['site-title']);
        Twitter::setSite($this->siteURL);

        ## Ou

        SEO::setTitle($settings['site-title']);
        SEO::setDescription($settings['site-description']);
        SEO::opengraph()->setUrl($this->siteURL);
        SEO::opengraph()->addProperty('type', 'articles');
        SEO::twitter()->setSite($this->siteURL);
    }
    
    public function index()
    {

        $latestPost = $this->post->getFrontLatestPost();

        // echo "<pre>";print_r($popularPost);exit;
        // SEO Data Method
        $this->SEOData();

        return view('Front.home.index', compact('latestPost','popularPost'));
    }

    public function howtowrite(){
        // SEO Data Method
        $this->SEOData();
        return view('Front.howtowrite');
    }

    public function about(){
        // SEO Data Method
        $this->SEOData();
        return view('Front.about');
    }

    public function policy(){
        // SEO Data Method
        $this->SEOData();
        return view('Front.policy');
    }

    public function contact(){
        // SEO Data Method
        $this->SEOData();
        return view('Front.contact');
    }

    public function details(){
        return view('Front.details');
    }

    public function createFeedback()
    {
         $input = Input::except(array('_token'));
         $validator = Validator::make($input, [
            'name' => 'required',
            'email' => 'required|email|unique:feedback,email',
            'message' => 'required',
        ]);

        if ($validator->passes()) {
            $this->feedback->AddFeedback($input);

            return Redirect::back()
                    ->with('success', 'Feedback Send Successfully.');
        }

        return Redirect::back()
                ->withInput()
                ->withErrors($validator->errors());
    }
    
    public function listCategoryPost($slug)
    {
        $categoryPost = $this->post->getPostWithCategory($slug);
        $subCategoryData = $this->subCategories->getSubCategorySlug($slug);

        SEOMeta::setTitle($subCategoryData->title);
        SEOMeta::setDescription($subCategoryData->meta_description);
        SEOMeta::addKeyword($subCategoryData->meta_keyword);

        OpenGraph::setDescription($subCategoryData->meta_description);
        OpenGraph::setTitle($subCategoryData->title);
        OpenGraph::setUrl($this->siteURL);
        OpenGraph::addProperty('type', 'articles');

        Twitter::setTitle($subCategoryData->title);
        Twitter::setSite($this->siteURL);

        ## Ou

        SEO::setTitle($subCategoryData->title);
        SEO::setDescription($subCategoryData->meta_description);
        SEO::opengraph()->setUrl($this->siteURL);
        SEO::opengraph()->addProperty('type', 'articles');
        SEO::twitter()->setSite($this->siteURL);

        return view('Front.categoryDetail',compact('categoryPost','subCategoryData'));
    }

    public function postDetails($category_slug,$post_slug)
    {
        $PostData = $this->post->getPostWithSlug($post_slug);

        if(!is_null($PostData)){
            $relatedPosts = $this->post->getRelatedPost($PostData->subcategory_id, $PostData->id);
        }

        SEOMeta::setTitle($PostData->title);
        SEOMeta::setDescription($PostData->meta_description);
        SEOMeta::addKeyword($PostData->meta_keyword);

        OpenGraph::setDescription($PostData->meta_description);
        OpenGraph::setTitle($PostData->title);
        OpenGraph::setUrl(URL::full());
        OpenGraph::addImage($this->siteURL.'/uploadImages/post/sample/'.$PostData->image_sample);

        Twitter::setTitle($PostData->title);
        Twitter::setSite(URL::full());

        ## Ou

        SEO::setTitle($PostData->title);
        SEO::setDescription($PostData->meta_description);
        SEO::opengraph()->setUrl(URL::full());
        SEO::opengraph()->addProperty('type', 'articles');
        SEO::twitter()->setSite(URL::full());

        DB::table('post')->where('id',$PostData->id)->increment('view', 1);

        return view('Front.details',compact('PostData','relatedPosts'));
    }

    public function SearchPost()
    {
        $input = Input::except(array('_token'));
        $searchText = $input['search'];

        // SEO Data Method
        $this->SEOData();

        if(!empty($searchText)){        
            $searchPostData = $this->post->getPostWithSearch($input['search']);
        }else{
            $searchPostData = '';
        }

        return view('Front.searchDetail',compact('searchPostData','searchText'));
    }

    public function PrintText($id)
    {
        $input = Input::except(array('_token'));

        if(empty($input['text'])){
            return back();
        }

        $PostData = $this->post->getPostWithId($id);

        if(!is_null($PostData)){
            $relatedPosts = $this->post->getRelatedPost($PostData->subcategory_id, $PostData->id);
        }   

        SEOMeta::setTitle($PostData->title);
        SEOMeta::setDescription($PostData->meta_description);
        SEOMeta::addKeyword($PostData->meta_keyword);

        OpenGraph::setDescription($PostData->meta_description);
        OpenGraph::setTitle($PostData->title);
        OpenGraph::setUrl(URL::full());
        OpenGraph::addProperty('type', 'articles');

        Twitter::setTitle($PostData->title);
        Twitter::setSite(URL::full());

        ## Ou

        SEO::setTitle($PostData->title);
        SEO::setDescription($PostData->meta_description);
        SEO::opengraph()->setUrl(URL::full());
        SEO::opengraph()->addProperty('type', 'articles');
        SEO::twitter()->setSite(URL::full());

        $img = Image::make('uploadImages/post/blank/'.$PostData->image_blank);

        $img->text($input['text'], $PostData->x_coordinate, $PostData->y_coordinate, function($font) use($PostData) {
            $font->file('uploadFontFile/'.Input::get('style',$PostData->fontFileName));
            $font->size(Input::get('size',$PostData->font_size));
            $font->color(Input::get('color',$PostData->font_color));
            $font->angle($PostData->rotate);
        });
	
        if($PostData->coordinate == 2){
            if(!empty($input['text2'])){
                $img->text($input['text2'], $PostData->x2_coordinate, $PostData->y2_coordinate, function($font) use($PostData) {
                    $font->file('uploadFontFile/'.Input::get('style',$PostData->fontFileName2));
            $font->size(Input::get('size',$PostData->font_size2));
            $font->color(Input::get('color',$PostData->font_color2));
                    $font->file('uploadFontFile/'.$PostData->fontFileName2);               
                    $font->angle($PostData->rotate2);
                });
            }
        }

        $printImageName = time().'_'.$PostData->image_blank;
        $img->save('textPrintImage/'.$printImageName);
        // $img->save(public_path('textPrintImage/'.$printImageName));

        DB::table('post')->where('id',$PostData->id)->increment('download', 1);

        OpenGraph::addImage($this->siteURL.'/textPrintImage/'.$printImageName);
        
        $printedName1 =  $input['text'];
        if(!empty($input['text2'])){
            $printedName2 =  $input['text2'];
        }else{
            $printedName2 =  '';
        }

        $fonts = DB::table('fonts')->lists("font_file","font_file");
        $fontSize = [];
        for ($i=15; $i < 101; $i++) { 
            $fontSize[$i] = $i;
        }

        return view('Front.downloadDetails',compact('PostData','printImageName','relatedPosts','printedName1','printedName2','fonts','fontSize'))
            ->with('siteURL',$this->siteURL);
    }

    public function getDownloadLink($printImageName)
    {
        $filepath = 'textPrintImage/'.$printImageName;
        return Response::download($filepath);
    }

    public function postDetailsName($name, $slug){
// return redirect()->route('front.categoryDetail','name-birthday-cakes');
        $PostData = $this->post->getPostWithIdName($slug);
        
        if(!is_null($PostData)){
            $relatedPosts = $this->post->getRelatedPost($PostData->subcategory_id, $PostData->id);
        }

        $PostNameData = $this->name->getPostNameData($name);

        if(empty($PostNameData)){
            return view('Front.details',compact('PostData','relatedPosts'));
        }

//if($PostData->subcategory_id != '22'){
  //          $PostData = $this->post->getPostNameDataRandom();
    //        return redirect()->route('front.namedetails',[$name, $PostData->slug]);
      //  }

        $description = str_replace("$$$", $name, $PostData->name_description);
        $keyword = str_replace("$$$", $name, $PostData->name_keywords);
        $titleName = str_replace("$$$", $name, $PostData->name_title);

        SEOMeta::setTitle($titleName);
        SEOMeta::setDescription($description);
        SEOMeta::addKeyword($keyword);

        OpenGraph::setDescription($description);
        OpenGraph::setTitle($titleName);
        OpenGraph::setUrl(URL::full());
        OpenGraph::addProperty('type', 'articles');

        Twitter::setTitle($titleName);
        Twitter::setSite(URL::full());

        ## Ou

        SEO::setTitle($titleName);
        SEO::setDescription($description);
        SEO::opengraph()->setUrl(URL::full());
        SEO::opengraph()->addProperty('type', 'articles');
        SEO::twitter()->setSite(URL::full());

        $img = Image::make(public_path('uploadImages/post/blank/'.$PostData->image_blank));

        $img->text($name, $PostData->x_coordinate, $PostData->y_coordinate, function($font) use($PostData) {
            $font->file('uploadFontFile/'.$PostData->fontFileName);
            $font->size($PostData->font_size);
            $font->color($PostData->font_color);                
            $font->angle($PostData->rotate);
        });

        $printImageName = $name.'_'.$PostData->id.'_'.$PostData->image_blank;
        $downloadImgPath = public_path('textPrintImage/'.$printImageName);
        if(!File::exists($downloadImgPath)){
            $img->save($downloadImgPath);
        }

        DB::table('post')->where('id',$PostData->id)->increment('download', 1);
        OpenGraph::addImage($this->siteURL.'/textPrintImage/'.$printImageName);

        $printedName = $name;
        return view('Front.detailsName',compact('PostData','printImageName','relatedPosts','printedName'))
            ->with('siteURL',$this->siteURL);
    }

    public function getDownloadLinkName($printImageName)
    {
        $filepath = 'textPrintImage/'.$printImageName;
        return Response::download($filepath);
    }

    public function generateNameImages(){
        ini_set('memory_limit', '1024M');
        set_time_limit(0); //60 seconds = 1 minute

        $postName = DB::table('post_name','names.name as Names')
                        ->join('names','names.id','=','post_name.name_id')
                        ->join('post','post.id','=','post_name.post_id')
                        ->leftjoin('fonts','fonts.id','=','post.font_type')
                        ->get();
         // add every post to the sitemap

         foreach ($postName as $key => $post)
         {
            $img = Image::make(public_path('uploadImages/post/blank/'.$post->image_blank));

            if(!empty($post->font_file)){
                $img->text($post->name, $post->x_coordinate, $post->y_coordinate, function($font) use($post) {
                    $font->file('uploadFontFile/'.$post->font_file);
                    $font->size($post->font_size);
                    $font->color($post->font_color);                
                    $font->angle($post->rotate);
                });

                $printImageName = $post->name.'_'.$post->id.'_'.$post->image_blank;
                $printImageNames = str_replace("/", '', $printImageName);
                $downloadImgPath = public_path('textPrintImageNames/'.$printImageNames);
                if(!File::exists($downloadImgPath)){
                    $img->save($downloadImgPath);
                }
            }
         }

        return Redirect::back();
    }

}
