<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Config;
use DB;

class PostName extends Model
{
    
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'post_name';

    protected $guarded = array();

    public $timestamps = false;

    public $fillable = ['post_id','name_id'];

    public function postAddNames($id, $name){
        
        $data = PostName::where('post_id',$id)->delete();

        foreach ($name as $key => $value) {
            $input = ['post_id'=>$id, 'name_id'=>$value];
            $data = PostName::create(array_only($input,$this->fillable));        
        }
    }

    public function getAddName($id){

        return DB::table('post_name')
                // DB::raw("(GROUP_CONCAT(post_name.name_id)) as nameId"))
                ->join('names','names.id','=','post_name.name_id')
                ->where('post_name.post_id',$id)
                ->lists('names.id','post_name.name_id');
                // ->get();
    }

}