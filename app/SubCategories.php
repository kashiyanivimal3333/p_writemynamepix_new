<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Config;
use DB;

class SubCategories extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sub_categories';

    protected $guarded = array();

    public function getSubCategories($input)
    {
        $data = static::select("sub_categories.*","categories.name as categoryName")
                    ->join('categories','categories.id','=','sub_categories.category_id');

        if (!empty($input['filter']) && is_array($input['filter'])) {
            foreach ($input['filter'] as $column => $row) {
                if (!empty($column) && !empty($row["value"]) && is_array($row)) {
                    $operator = Config::get("setting.type", 1)[$row["type"]];
                    if ($row["type"] == 7) {
                        $data->where('sub_categories.'.$column, $operator, "%{$row["value"]}%");
                    } else {
                        $data->where('sub_categories.'.$column, $operator, $row["value"]);
                    }
                }
            }
        }

        return $data = $data->paginate(10);
    }

    public function AddSubCategory($input)
    {
        return static::create($input);
    }

    public function editSubCategory($input)
    {
        return static::where('id',$input['id'])->update(array_except($input,array('id')));
    }

    public function removeSubCategory($id)
    {
        return static::where('id',$id)->delete();
    }

    public function getSubCategoryList()
    {
        return DB::table('sub_categories')->lists('name','id');
    }

    public function getTotalSubCategory()
    {
        return static::count();
    }

    public function getSubCategorySlug($slug)
    {
        return static::where('slug',$slug)->first();
    }

    public function getFrontCategoriesNew()
    {
        return static::where('is_new',1)->first();
    }
}
