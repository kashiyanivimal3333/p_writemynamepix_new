<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Config;
use DB;

class Slider extends Model
{
    

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'slider';

    protected $guarded = array();

    public function getSlider($input)
    {
        $data = static::select("slider.*");

        return $data = $data->paginate(10);
    }

    public function AddSlider($input)
    {
        return static::create($input);
    }

    public function removeSlider($id)
    {
        return static::where('id',$id)->delete();
    }

    public function getTotalSliderImage()
    {
        return static::count();
    }

    public function getFrontSliderImage()
    {
        return static::get();
    }
}
