@extends($theme)

@section('pageTitle')
<title>Post</title>
@endsection

@section('style')
<link href="{{ asset('/adminTheme/bootstrap-colorpicker-master/dist/css/bootstrap-colorpicker.min.css') }}" rel="stylesheet">
@endsection

@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h1 class="page-header">
            Post Management
            </h1>
        </div>
        <div class="pull-right">
            <button class="btn btn-success" data-toggle="modal" data-target="#create-post">Create New Post</button>
            <button class="btn btn-primary search-modules">Search</button>
            @include('post.create')
        </div>
    </div>
</div>

@include('post.search')

<table class="table table-bordered pagin-table">
    <thead>
        <tr>
            <th>No.</th>
            <th>Sample Image</th>
            <th>Title</th>
            <th>Total Download</th>
            <th>Total View</th>
            <th>Status</th>
            <th width="250px">Action</th>
        </tr>
    </thead>
    <tbody>
        @include('post.data')
    </tbody>
</table>

@include('adminTheme.paginate')
@endsection

@section('script')
<script src="{{ asset('/adminTheme/bootstrap-colorpicker-master/dist/js/bootstrap-colorpicker.js') }}"></script>

<script type="text/javascript">
    $(function() {
      $('.morefiles').on('change', gotPic);
    });
    

    function gotPic(event) {
      if (event.target.files.length) {
        for (var i = 0; i < event.target.files.length; i++) {
          if (event.target.files[i].type.indexOf('image/') == 0) {
            var src = URL.createObjectURL(event.target.files[i])
            
            $(this).parents('.modal').find('.preview').html('');
            $(this).parents('.modal').find('.preview').html('<img src = ' + src + '><br>');
          }
        }
      }
    }

    $("body").on("click",".preview img",function(e){

        var offset = $(this).offset();

        if ($(this).parents('.preview-parent').find(".firstcoo").prop("checked")) {
            $(this).parents('.preview-parent').find(".x").val(parseInt(e.pageX - offset.left));
            $(this).parents('.preview-parent').find(".y").val(parseInt(e.pageY - offset.top));   
        }
        if ($(this).parents('.preview-parent').find(".secondcoo").prop("checked")) {
            $(this).parents('.preview-parent').find(".x2").val(parseInt(e.pageX - offset.left));
            $(this).parents('.preview-parent').find(".y2").val(parseInt(e.pageY - offset.top));     
        }
        
    });
</script>

<script>
    $(function(){
        $('.demo2').colorpicker();
    });
</script>

@endsection
