@extends($theme)

@section('pageTitle')
<title>Post Add Name</title>
@endsection

@section('style')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/chosen/1.5.1/chosen.min.css">    
@endsection

@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h1 class="page-header">
            Add Name In Post
            </h1>
        </div>
        <div class="pull-right">
            <a href="{{ URL::route('admin.post') }}"><button class="btn btn-danger">Back</button></a>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-6">
        {!! Form::open(array('route' => 'admin.post.create.addname','method'=>'post')) !!}
        
        {!! Form::hidden('id', $id) !!}
        {!! Form::select('name_id[]', $getName, $getAddName, array('class' => 'form-control chosen', 'multiple'=>'multiple')) !!}
        
        <br>
        @if($is_all == 1)
        <input type="checkbox" name="is_all" value="" checked> Select All Names
        @else
        <input type="checkbox" name="is_all" value="" > Select All Names
        @endif

        <br>
        <br>
        <button type="submit" class="btn btn-primary">Submit Name</button>
        {!! Form::close() !!}
    </div>    
</div>

@endsection

@section('script')
<script src="{{ asset('/adminTheme/js/chosen.jquery.min.js') }}"></script>
<script type="text/javascript">
      $(".chosen").chosen();
</script>
@endsection
