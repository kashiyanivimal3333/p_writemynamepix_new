<!-- Modal -->
<div class="modal fade" id="postKeyword-{{ $value->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="width:650px;">
        <div class="modal-content">
            {!! Form::open(array('route' => 'admin.postkeyword.edit','autocomplete'=>'off')) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Post Keywords</h4>
            </div>
            <div class="modal-body">
            	<div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-danger print-error-msg" style="display:none">
                        <ul></ul>
                    </div>
                    {!! Form::hidden('id',$value->id) !!}
                    {!! Form::hidden('sitemap',$value->sitemap) !!}
                    <div class="row">    
                        <div class="col-md-12">
                            <label class="form-lable">Enter Post Title</label>
                            {!! Form::textarea('name_title', Input::get('name_title',$value->name_title), array('placeholder' => 'description','class' => 'form-control user-name-edit','id'=>'editor1','style'=>'height:100px;')) !!}
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <label class="form-lable">Enter Post Keywords</label>
                            {!! Form::textarea('name_keywords', Input::get('name_keywords',$value->name_keywords), array('placeholder' => 'description','class' => 'form-control user-name-edit','id'=>'editor1','style'=>'height:100px;')) !!}
                        </div>
                    </div>
                    <div class="row">    
                        <div class="col-md-12">
                            <label class="form-lable">Enter Post Description</label>
                            {!! Form::textarea('name_description', Input::get('name_description',$value->name_description), array('placeholder' => 'description','class' => 'form-control user-name-edit','id'=>'editor1','style'=>'height:100px;')) !!}
                        </div>
                    </div>
                    <br>
                    <div class="row">    
                        <div class="col-md-12">
		                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		                    <button type="submit" class="btn btn-primary create-crud">Submit</button>
                    	</div>
                    </div>	
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
</div>            