<!-- Modal -->
<div class="modal fade" id="change-post-status-{{ $value->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="width:650px;">
        <div class="modal-content">
            {!! Form::open(array('route' => 'admin.changePostStatus.update','autocomplete'=>'off')) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Chnage Post Status</h4>
            </div>
            <div class="modal-body">
            	<div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-danger print-error-msg" style="display:none">
                        <ul></ul>
                    </div>
                    {!! Form::hidden('id',$value->id) !!}
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="form-lable">Title</label>
                            {!! Form::text('title', Input::get('title',$value->title), array('placeholder' => 'title','class' => 'form-control')) !!}
                        </div>
                    </div>
                    <div class="row">    
                        <div class="col-md-12" style="padding-left: 30px;">
                            <label class="form-lable">Status</label>
                            <div class="radio">
                                <label>
                                {!! Form::radio('status', 0,$value->status == 0 ? 'true':'') !!} Deactive
                                </label>
                            </div>
                            <div class="radio">
                                <label>
                                {!! Form::radio('status', 1,$value->status == 1 ? 'true':'') !!} Active
                                </label>
                            </div>
                        </div>
                    </div>
                    <br>
                    <div class="row">    
                        <div class="col-md-12">
		                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
		                    <button type="submit" class="btn btn-primary create-crud">Submit</button>
                    	</div>
                    </div>	
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>            
</div>