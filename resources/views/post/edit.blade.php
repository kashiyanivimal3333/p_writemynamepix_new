<!-- Modal -->
<div class="modal fade" id="post-{{ $value->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document" style="width:1060px;">
        <div class="modal-content">
            {!! Form::open(array('route' => 'admin.post.edit','autocomplete'=>'off')) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Post</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-danger print-error-msg" style="display:none">
                        <ul></ul>
                    </div>
                    {!! Form::hidden('id',$value->id) !!}
                    <div class="row">
                        <div class="col-xs-4 col-sm-4 col-md-4">
                            <div class="form-group">
                                <label class="form-lable">Select SubCategory</label>
                                {!! Form::select('subcategory_id',array(''=>'select subcategory')+$subCategoryList, $value->subcategory_id, array('class' => 'form-control user-name-edit')) !!}
                            </div>
                        </div> 
                        <div class="col-xs-4 col-sm-4 col-md-4">
                            <div class="form-group">
                                <label class="form-lable">Title</label>
                                {!! Form::text('title', Input::get('title',$value->title), array('placeholder' => 'title','class' => 'form-control')) !!}
                            </div>
                        </div>
                        <div class="col-xs-4 col-sm-4 col-md-4">
                            <div class="form-group">
                                <label class="form-lable">Select Sample Image</label>
                                {!! Form::file('image_sample', array('class' => 'form-control')) !!}
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-lable">Select Blank Image</label>
                                {!! Form::file('image_blank', array('class' => 'form-control morefiles')) !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-lable">Enter Your Name</label>
                                {!! Form::text('enter_your_name', Input::get('enter_your_name',$value->enter_your_name), array('placeholder' => 'enter your name','class' => 'form-control')) !!}
                            </div>
                        </div>
                    </div>

                    <div class="row preview-parent">
                        <div class="col-md-6">
                            <div class="preview"><img src="/uploadImages/post/blank/{!! $value->image_blank !!}" style="height:500px;width:500px;"></div>
                        </div>
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group coordinate-radio">
                                        <input type="radio" name="coordinate" value="1" class="firstcoo" {!! $value->coordinate == '1' ? 'checked=true':'' !!}> First Coordinate
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-lable">Charater Limit</label>
                                        {!! Form::text('char_limit', Input::get('char_limit',$value->char_limit), array('placeholder' => 'charater limit','class' => 'form-control')) !!}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-lable">Name Rotate</label>
                                        {!! Form::text('rotate', Input::get('rotate',$value->rotate), array('placeholder' => 'rotate','class' => 'form-control')) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-lable">X-Coordinate</label>
                                        {!! Form::text('x_coordinate', Input::get('x_coordinate',$value->x_coordinate), array('placeholder' => 'x-coordinate','class' => 'form-control x')) !!}
                                    </div>    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-lable">Y-Coordinate</label>
                                        {!! Form::text('y_coordinate', Input::get('y_coordinate',$value->y_coordinate), array('placeholder' => 'y-coordinate','class' => 'form-control y')) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-lable">Font Size</label>
                                        {!! Form::text('font_size', Input::get('font_size',$value->font_size), array('placeholder' => 'font size','class' => 'form-control')) !!}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-lable">Font Color</label>
                                        <div class="input-group demo2">
                                            <input type="text" name="font_color" value="{!! $value->font_color !!}" class="form-control" />
                                            <span class="input-group-addon"><i></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-lable">Select Font Type</label>
                                        {!! Form::select('font_type',array(''=>'select font type')+$fontFileList, $value->font_type, array('class' => 'form-control user-name-edit')) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group coordinate-radio">
                                        <input type="radio" name="coordinate" value="2" class="secondcoo" {!! $value->coordinate == '2' ? 'checked=true':'' !!}> Second Coordinate
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-lable">Charater Limit</label>
                                        {!! Form::text('char_limit2', Input::get('char_limit2',$value->char_limit2), array('placeholder' => 'charater limit','class' => 'form-control')) !!}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-lable">Name Rotate</label>
                                        {!! Form::text('rotate2', Input::get('rotate2',$value->rotate2), array('placeholder' => 'rotate','class' => 'form-control')) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-lable">X-Coordinate</label>
                                        {!! Form::text('x2_coordinate', Input::get('x_coordinate',$value->x2_coordinate), array('placeholder' => 'x-coordinate','class' => 'form-control x2')) !!}
                                    </div>    
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-lable">Y-Coordinate</label>
                                        {!! Form::text('y2_coordinate', Input::get('y_coordinate',$value->y2_coordinate), array('placeholder' => 'y-coordinate','class' => 'form-control y2')) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-lable">Font Size</label>
                                        {!! Form::text('font_size2', Input::get('font_size',$value->font_size), array('placeholder' => 'font size','class' => 'form-control')) !!}
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="form-lable">Font Color</label>
                                        <div class="input-group demo2">
                                            <input type="text" name="font_color2" value="{!! $value->font_color2 !!}" class="form-control" />
                                            <span class="input-group-addon"><i></i></span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="form-lable">Select Font Type</label>
                                        {!! Form::select('font_type2',array(''=>'select font type')+$fontFileList, $value->font_type2, array('class' => 'form-control user-name-edit')) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                            <label class="form-lable">Enter Description</label>
                            {!! Form::textarea('description', Input::get('description',$value->description), array('placeholder' => 'description','class' => 'form-control user-name-edit','id'=>'editor1','style'=>'height:100px;')) !!}
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <label class="form-lable">Meta Keyword</label>
                                {!! Form::textarea('meta_keyword', Input::get('meta_keyword',$value->meta_keyword), array('placeholder' => 'meta keyword','class' => 'form-control user-name-edit','style'=>'height:100px;')) !!}
                            </div>
                        </div>
                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <label class="form-lable">Meta Description</label>
                                {!! Form::textarea('meta_description', Input::get('meta_description',$value->meta_description), array('placeholder' => 'meta description','class' => 'form-control user-name-edit','style'=>'height:100px;')) !!}
                            </div>
                        </div>

                        <div class="col-xs-6 col-sm-6 col-md-6">
                            <div class="form-group">
                                <label class="form-lable">Is Alpah</label>
                                @if($value->is_alpha == 1)
                                    <label>{!! Form::radio('is_alpha','1', true, array('class' => 'form-control')) !!} Yes</label>
                                    <label>{!! Form::radio('is_alpha','0', false, array('class' => 'form-control')) !!} No</label>
                                @else
                                    <label>{!! Form::radio('is_alpha','1', false, array('class' => 'form-control')) !!} Yes</label>
                                    <label>{!! Form::radio('is_alpha','0', true, array('class' => 'form-control')) !!} No</label>
                                @endif
                            </div>
                        </div>

                    </div>

                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary create-crud">Submit</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
</div>