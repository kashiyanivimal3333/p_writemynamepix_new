@if(!empty($displayData) && $displayData->count())
    @foreach($displayData as $key => $value)
        <tr>
            <td>{{ ++$i }}</td>
            <td><img src="/uploadImages/post/sample/{!! $value->image_sample !!}" style="height:100px;width:100px;"></td>
            <td>{{ $value->title }}</td>
            <td>{{ $value->download }}</td>
            <td>{{ $value->view }}</td>
            <td>
                @if($value->status == 1)
                    <label class="label label-success">Active</label>
                @else
                    <label class="label label-danger">Deactive</label>
                @endif
            </td>
            <td>
                <button class="btn edit-post btn-primary" data-toggle="modal" data-target="#post-{{ $value->id }}">Edit</button>
                @include('post.edit')
                <button class="btn btn-danger remove-crud" data-id="{{ $value->id }}" data-action="{{ URL::route('admin.post.remove') }}">Delete</button>
                
                <a href="{{ URL::route('admin.post.addname',['id'=>$value->id,'is_all'=>$value->is_all]) }}"><button class="btn btn-info">Add Name</button></a>

                <button class="btn btn-info" data-toggle="modal" data-target="#change-post-status-{{ $value->id }}" style="margin-top: 10px;">Change Status</button>
                @include('post.changeStatus')
                
                <button class="btn btn-info" data-toggle="modal" data-target="#postKeyword-{{ $value->id }}" style="margin-top: 10px;">Name Keywords</button>
                @include('post.editNameKeyword')
                
            </td>
        </tr>
    @endforeach
@endif