<div class="panel panel-default filter-panel {{Input::get("filter",false)?'filter-in':'filter-out'}}">
    <div class="panel-heading">
        <h3 class="panel-title">Searching</h3>
    </div>
    <div class="panel-body">
        {!! Form::open(array('method'=>'get','class'=>'form-filter form-inline')) !!}
        <ul class="search">
            <li>
                <div class='form-group'>
                    {!! Form::label('filter[title][value]', 'Title',array('class'=>'control-label')) !!}
                    {!! Form::text('filter[title][value]',Input::get('filter.title.value'),array('class'=>'search-crud form-control')) !!}
                    {!! Form::hidden('filter[title][type]','7') !!}
                </div>
            </li>
        </ul>
        {!! Form::close() !!}
    </div>
</div>
