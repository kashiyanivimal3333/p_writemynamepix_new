@extends($theme)

@section('pageTitle')
<title>Names</title>
@endsection

@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h1 class="page-header">
            Names Management
            </h1>
        </div>
        <div class="pull-right">
            <button class="btn btn-success" data-toggle="modal" data-target="#create-name">Create Name</button>
            <button class="btn btn-primary search-modules">Search</button>
            @include('name.create')
        </div>
    </div>
</div>
@include('name.search')
<table class="table table-bordered pagin-table">
    <thead>
        <tr>
            <th style="width: 80px;">No</th>
            <th>Name</th>
        </tr>
    </thead>
    <tbody>
    @include('name.data')
    </tbody>
</table>
@include('adminTheme.paginate')
@endsection