@extends($FrontThemeNew)

@section('pageTitle')
<title>About Us</title>
@stop

@section('pageContent')

<h1 class="page-heading">About Us</h1>

<div class="row details-box">
    <div class="col-md-12">
        <div class="desc-details">
            {!! $settings['aboutUs'] !!}
        </div>
    </div>
</div>

@stop
