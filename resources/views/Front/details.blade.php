@extends($FrontThemeNew)

@section('pageTitle')
<title>{!! $PostData->title !!}</title>
@stop

@section('PageLevelStyle')
<link rel="stylesheet" href="{{ asset('/frontTheme/css/shr.css') }}">
<style type="text/css">

    .examples .button svg{
        fill: currentcolor;
        height: 16px;
        margin-right: 7px;
        vertical-align: -3px;
        width: 16px;
    }

    .examples .button svg{
        fill: currentcolor;
    }
</style>
@stop

@section('pageContent')
<h1 class="page-heading">{!! ucfirst($PostData->title) !!}</h1>

<div class="row">
    <div class="col-md-12">
        <div class="desc-details">
            <p>{!! $PostData->description !!}</p>
            <p>Want to write your name on <b>{!! $PostData->title !!}</b> pictures? we have provide a lot of new and inique pics for you. My Site Birthday Wish Name Cakes also allow to generate your name on your favourite pictures. you can write your name in this Dolls pictures by following step of this link <b><a href="{{ URL::route('front.howtowrite') }}" target="blanck"> How To Create </a></b>.</p>             
            <p>After generating picture with your name on Cute pictures you can share it with your friends on social networks like Google, Facebook, Twitter and others. you can also download your generated picture directly to your PC. Hope you will enjoy creating your names on pictures.</p>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-7 details-img">
        <img src="/uploadImages/post/sample/{!! $PostData->image_sample !!}" class="img-responsive">
    </div>
    <div class="col-md-5 details-text">
        {!! Form::open(array('route' => array('front.printText.get',$PostData->id),'method'=>'get','autocomplete'=>'off')) !!}
        <div class="row">
            <div class="form-group col-md-12">
               <label id="charLimit" attr-char-limit-value={!! $PostData->char_limit !!}>Write His / Her Name Here.</label> 
                <div class="row">
                    <div class="col-md-6 col-sm-6 char_limit">
                        Character Limit : {!! $PostData->char_limit !!}
                    </div>
                    <div class="col-md-6 col-sm-6 char-left">
                        Left Character : <label class="jleft_char">{!! $PostData->char_limit !!}</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <input class="form-control" name="text" type="text" id="dis_name">
                    </div>
                </div>
            </div>
        </div>
        @if($PostData->coordinate == 2)
        <div class="row">
            <div class="form-group col-md-12">
               <label id="charLimit2" attr-char-limit-value={!! $PostData->char_limit2 !!}>Write Your Good Name Here.</label> 
                <div class="row">
                    <div class="col-md-6 col-sm-6 char_limit">
                        Character Limit : {!! $PostData->char_limit2 !!}
                    </div>
                    <div class="col-md-6 col-sm-6 char-left">
                        Left Character : <label class="jleft_char2">{!! $PostData->char_limit2 !!}</label>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 col-sm-12">
                        <input class="form-control" name="text2" type="text" id="dis2_name">
                    </div>
                </div>
            </div>
        </div>
        @endif

        <div class="row details-text-btn">
            <div class="col-md-12">
                <button type="submit" class="btn btn-block btn-success">Create Image</button>
            </div>
        </div>
        {!! Form::close() !!}
    </div>
</div>

<br />
<div class="row">
    <div class="col-md-12" align="center">
        <strong> Share It On </strong>
        <br /><br />
        <div class="a2a_kit a2a_kit_size_32 a2a_default_style">
    <a class="a2a_button_facebook a2a_counter"></a>
    <a class="a2a_button_pinterest a2a_counter"></a>
    <a class="a2a_button_linkedin a2a_counter"></a>
    <a class="a2a_button_tumblr a2a_counter"></a>
    <a class="a2a_button_reddit a2a_counter"></a>
    <a class="a2a_dd a2a_counter" href="https://www.addtoany.com/share"></a>
</div>

<script async src="https://static.addtoany.com/menu/page.js"></script>

    </div>
</div>

@stop

@section('PageLevelScript')

<script type="text/javascript">

    $( document ).ready(function() {
        $('#dis_name').val('');
        var limitChar = $('#charLimit').attr('attr-char-limit-value') - $('#dis_name').val().length;
        $('.jleft_char').text(limitChar);

        $('#dis2_name').val('');
        var limitChar2 = $('#charLimit2').attr('attr-char-limit-value') - $('#dis2_name').val().length;
        $('.jleft_char2').text(limitChar2);
    });

    $('#dis_name').keyup(function() {
        var limitChar = $('#charLimit').attr('attr-char-limit-value');
        document.getElementById("dis_name").maxLength = limitChar;

        var characterLeftSortD = limitChar - $('#dis_name').val().length;
        $('.jleft_char').text(characterLeftSortD);
    });

    $('#dis2_name').keyup(function() {
        var limitChar2 = $('#charLimit2').attr('attr-char-limit-value');

        document.getElementById("dis2_name").maxLength = limitChar2;

        var characterLeftSort2 = limitChar2 - $('#dis2_name').val().length;
        $('.jleft_char2').text(characterLeftSort2);
    });
</script>
<div id="fb-root"></div>

<script>
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.5&appId=1673475329590948";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>


<!-- Load SVG defs -->
<!-- You should bundle all SVG/Icons into one file using a build tool such as gulp and svg store -->
<script>
(function(d, u){
    var x = new XMLHttpRequest(),
        b = d.body;

    // Check for CORS support
    // If you're loading from same domain, you can remove the if statement
    // XHR for Chrome/Firefox/Opera/Safari
    if ("withCredentials" in x) {
        x.open("GET", u, true);
    }
    // XDomainRequest for older IE
    else if(typeof XDomainRequest != "undefined") {
        x = new XDomainRequest();
        x.open("GET", u);
    }
    else {
        return;
    }

    x.send();
    x.onload = function(){
        var c = d.createElement("div");
        c.setAttribute("hidden", "");
        c.innerHTML = x.responseText;
        b.insertBefore(c, b.childNodes[0]);
    }
})(document, "https://cdn.shr.one/0.1.9/sprite.svg");
</script>

<!-- Shr core script -->
<script src="{{ asset('/frontTheme/js/shr.js') }}"></script>
<!-- Docs script -->
<script src="{{ asset('/frontTheme/js/docs.js') }}"></script>
@stop
