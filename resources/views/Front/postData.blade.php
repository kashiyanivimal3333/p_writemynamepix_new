<div class="col-md-4 portfolio-item">
    <a href="{!! URL::route('front.details',[$value->subCategorySlug,$value->slug]) !!}">
        <!-- <img class="img-responsive" src="http://placehold.it/700x400" alt=""> -->
        <img src="/uploadImages/post/sample/{!! $value->image_sample !!}" alt="{{ $value->title }}">
    </a>
    <h5><a href="{!! URL::route('front.details',[$value->subCategorySlug,$value->slug]) !!}">{!! ucfirst($value->title) !!}</a></h5>
</div>
