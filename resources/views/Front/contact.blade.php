@extends($FrontThemeNew)

@section('pageTitle')
<title>Contact Us</title>
@stop

@section('pageContent')
    
    <h1 class="page-heading">Contact Us</h1>
    
    <div class="row details-box">
        <div class="col-md-8 contact-form">
            <h4>Your Feedback</h4>
            @if ($message = Session::get('success'))
            <div class="alert alert-success alert-block">
                <button type="button" class="close" data-dismiss="alert">&times;</button>   
                    <strong>{{ $message }}</strong>
            </div>
            @endif
            {!! Form::open(array('route' => 'front.feedback.create', 'data-parsley-validate', 'autocomplete'=>'off','class'=>'form-horizontal')) !!}
                <div class="form-group">
                   <label class="col-sm-3 control-label">Name<sup>*</sup> :</label>
                    <div class="col-sm-8">                        
                        {!! Form::text('name',Input::get('name'),array('class'=>'form-control','placeholder'=>'First &amp; Last Name','required'=>'required','data-parsley-required-message'=>'Name is required')) !!}
                    </div>
                </div>

                <div class="form-group">
                   <label class="col-sm-3 control-label">Email<sup>*</sup> :</label>
                    <div class="col-sm-8">
                        {!! Form::email('email',Input::get('email'),array('class'=>'form-control','placeholder'=>'example@xyz.com','required'=>'required','data-parsley-required-message'=>'Email is required')) !!}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Message<sup>*</sup> :</label>
                    <div class="col-sm-8">
                        {!! Form::textarea('message',Input::get('message'),array('class'=>'form-control','placeholder'=>'Enter Message','required'=>'required','data-parsley-required-message'=>'Massage is required','style'=>'max-height:100px;')) !!}
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-3 control-label">Word :</label>
                    <div class="col-sm-8">
                        {!! Form::textarea('word',Input::get('word'),array('class'=>'form-control','placeholder'=>'Enter Word','style'=>'max-height:100px;')) !!}
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-9 col-sm-offset-3 details-text-btn">
                        <button class="btn btn-success" type="submit">Submit</button>
                    </div>
                </div>

            {!! Form::close() !!}
        </div>
        <div class="col-md-4 contact-details">
            <h4>Contact Details</h4>
            <div class="row">
                <div class="col-md-12 contact-icon">
                    <span aria-hidden="true" class="glyphicon glyphicon-globe"></span>
                    <a href="#">www.exampale.com</a>
                </div>
                <div class="col-md-12 contact-icon">
                    <span aria-hidden="true" class="glyphicon glyphicon-envelope"></span>
                    {!! $settings['email'] !!}
                </div>
                <div class="col-md-12 contact-icon">
                    <span aria-hidden="true" class="glyphicon glyphicon-map-marker"></span>
                    {!! $settings['city'] !!}.
                </div>
            </div>
        </div>
    </div>
@stop

@section('PageLevelScript')
<script>
    window.ParsleyConfig = {
        errorsWrapper: '<div></div>',
        errorTemplate: '<div class="alert alert-danger parsley" role="alert"></div>',
        errorClass: 'has-error',
        successClass: 'has-success'
    };
</script>
<script src="{{ asset('/js/parsley.js') }}"></script>
@stop
