@extends($FrontTheme)

@section('pageTitle')
<?php $maintitle = str_replace("$$$", $printedName, $PostData->name_title); ?>
<title>{!! $maintitle !!}</title>
@stop

@section('pageContent')
<div class="col-md-9 page-content-right">
    <div class="row headding">
        <div class="col-md-12">
            <hr class="colorgraph">
            <h4>{!! $maintitle !!}</h4>
        </div>
    </div>  


    <div class="row details-box">
        <div class="col-md-12">
            <div class="desc-details">
                Want to write your name on <b>{!! $maintitle !!}</b> pictures? we have provide a lot of new and inique pics for you. My Site Birthday Wish Name Cakes also allow to generate your name on your favourite pictures. you can write your name in this Dolls pictures by following step of this link <b><a href="{{ URL::route('front.howtowrite') }}" target="blanck"> How To Create </a></b>. 
            </div>
        </div>
    </div> 
    <div class="row pageContent-add">
        <div class="col-md-12">
            {!! $settings['above-postimage'] !!}
        </div>
    </div>
    <div class="row details-box">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-7 details-img">
                    <img src="/textPrintImage/{!! $printImageName !!}">
                </div>
                <div class="col-md-5 details-text">
                    {!! Form::open(array('route' => array('front.printText.get',$PostData->id),'method'=>'get','autocomplete'=>'off')) !!}
                    <div class="row">
                        <div class="form-group col-md-12">
                           <label id="charLimit" attr-char-limit-value={!! $PostData->char_limit !!}>Write His / Her Name Here.</label> 
                            <div class="row">
                                <div class="col-md-6 col-sm-6 char_limit">
                                    Character Limit : {!! $PostData->char_limit !!}
                                </div>
                                <div class="col-md-6 col-sm-6 char-left">
                                    Left Character : <label class="jleft_char">{!! $PostData->char_limit !!}</label>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12 col-sm-12">
                                    <input class="form-control" name="text" value="{!! $printedName !!}" type="text" id="dis_name">
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="alert alert-success fade in">
					    <a title="close" aria-label="close" data-dismiss="alert" class="close" href="#">×</a>
					    <strong>Success!</strong> Now you can download image.
					</div>
                    <div class="row details-text-btn">
                        <div class="col-md-12">
                            <button type="submit" class="btn btn-block">Create Image With Change</button>
                        </div>
                    </div><br>
                    <div class="row details-text-btn">
                        <div class="col-md-12">
                            <a href="{!! URL::route('front.downloadName.get',$printImageName) !!}" class="btn btn-block">Download</a>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>   


    <div class="row pageContent-add">
        <div class="col-md-12">
            {!! $settings['after-textbox'] !!}
        </div>
    </div>

    <br>


    @if(!empty($relatedPosts) && $relatedPosts->count())
    <div class="row headding">
        <div class="col-md-12">
            <hr class="colorgraph">
            <h4>Related Write Name Images</h4>
        </div>
    </div>   
    <div class="row">
        @foreach($relatedPosts as $key=>$value)
          @if($key <= 1)
            @include('Front.postData')
         @endif
        @endforeach
    </div>    
    @endif

    <!-- <div class="row headding">
        <div class="col-md-12">
            <hr class="colorgraph">
            <h4>Facebook Comments</h4>
        </div>
    </div>    
    <br />
    <div class="fb-comments" data-href="https://www.facebook.com/Birthdaywishnamecakes/" data-numposts="5"></div> -->          
</div>    
@stop

@section('PageLevelScript')
<script type="text/javascript">

    $( document ).ready(function() {
    	var printedName = "<?php echo $printedName; ?>";
        $('#dis_name').val(printedName);
        var limitChar = $('#charLimit').attr('attr-char-limit-value') - $('#dis_name').val().length;
        $('.jleft_char').text(limitChar);
    });

    $('#dis_name').keyup(function() {
        var limitChar = $('#charLimit').attr('attr-char-limit-value');
        document.getElementById("dis_name").maxLength = limitChar;

        var characterLeftSortD = limitChar - $('#dis_name').val().length;
        $('.jleft_char').text(characterLeftSortD);
    });
</script>
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.5&appId=1673475329590948";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
@stop