@extends($FrontThemeNew)

@section('pageTitle')
<title>{!! $subCategoryData->title !!}</title>
@stop

@section('pageContent')

<h1 class="page-heading">{!! ucfirst($subCategoryData->name) !!}</h1>

<p>{{ $subCategoryData->meta_description }}</p>
<hr>
<!-- Projects Row -->
<div class="row">
    @if(!empty($categoryPost) && $categoryPost->count())
        @foreach($categoryPost as $key=>$value)
            @include('Front.postData')
        @endforeach
    @endif
</div>
<!-- /.row -->

<hr>

<!-- Pagination -->
{!! $categoryPost->render() !!}
<!-- /.row -->
@stop
