@extends($FrontThemeNew)

@section('pageTitle')
<title>How To Write Name</title>
@stop

@section('pageContent')

<h1 class="page-heading">How To Write Name</h1>

<div class="row details-box">
    <div class="col-md-12">
        <p><strong> Step - 1 : </strong>Select any pictures from that list. </p>
    </div>
    <div class="col-md-12 text-center">
        <img src="/images/howtowrite-1.jpg">
    </div>
</div>
<div class="row details-box">
    <div class="col-md-12">
        <p><strong> Step - 2 : </strong>Write your name in the textboxs. Then press the “Create image” button. </p>
    </div>
    <div class="col-md-12 text-center">
        <img src="/images/howtowrite-2.jpg" width="100%">
    </div>
</div>
<div class="row details-box">
    <div class="col-md-12"> 
        <p><strong> Step - 3 : </strong>Name generated on selected image. Click on “Download” button to download image. Now you can download image. </p>
    </div>
    <div class="col-md-12 text-center">
        <img src="/images/howtowrite-3.jpg" width="100%">
    </div>
</div>

@stop
