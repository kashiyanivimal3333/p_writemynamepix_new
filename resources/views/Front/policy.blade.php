@extends($FrontThemeNew)

@section('pageTitle')
<title>Privacy Policy</title>
@stop

@section('pageContent')

<h1 class="page-heading">Privacy Policy</h1>

<div class="row details-box">
    <div class="col-md-12">
        <div class="desc-details">
            <strong>Personal identification information</strong>
            <p>We may collect personal identification information from Users in a variety of ways, including, but not limited to, when Users visit our site, subscribe to the newsletter, and in connection with other activities, services, features or resources we make available on our Site. Users may be asked for, as appropriate, name. Users may, however, visit our Site anonymously. We will collect personal identification information from Users only if they voluntarily submit such information to us. Users can always refuse to supply personally identification information, except that it may prevent them from engaging in certain Site related activities.</p>
            <strong>Non-personal identification information</strong>
            <p>We may collect non-personal identification information about Users whenever they interact with our Site. Non-personal identification information may include the browser name, the type of computer and technical information about Users means of connection to our Site, such as the operating system and the Internet service providers utilized and other similar information.</p>
            <strong>Web browser cookies</strong>
            <p>Our Site may use "cookies" to enhance User experience. User's web browser places cookies on their hard drive for record-keeping purposes and sometimes to track information about them. User may choose to set their web browser to refuse cookies, or to alert you when cookies are being sent. If they do so, note that some parts of the Site may not function properly.</p>
            <strong>How we use collected information</strong>
            <p>Writemynamepix may collect and use Users personal information for the following purposes:</p>
            <ul>
                <li>To personalize user experience We may use information in the aggregate to understand how our Users as a group use the services and resources provided on our Site.</li>
                <li>To send periodic emails If User decides to opt-in to our mailing list, they will receive emails that may include company news, updates, related product or service information, etc. If at any time the User would like to unsubscribe from receiving future emails, we include detailed unsubscribe instructions at the bottom of each email.</li>
            </ul>
            <strong>How we protect your information</strong>
            <p>We adopt appropriate data collection, storage and processing practices and security measures to protect against unauthorized access, alteration, disclosure or destruction of your personal information, username, password, transaction information and data stored on our Site.</p>
            <strong>Sharing your personal information</strong>
            <p>We do not sell, trade, or rent Users personal identification information to others. We may share generic aggregated demographic information not linked to any personal identification information regarding visitors and users with our business partners, trusted affiliates and advertisers for the purposes outlined above.</p>
            <strong>Google Adsense</strong>
            <p>Some of the ads may be served by Google. Google's use of the DART cookie enables it to serve ads to Users based on their visit to our Site and other sites on the Internet. DART uses "non personally identifiable information" and does NOT track personal information about you, such as your name, email address, physical address, etc. You may opt out of the use of the DART cookie by visiting the Google ad and content network privacy policy at <a href="http://www.google.com/privacy_ads.html"> http://www.google.com/privacy_ads.html</a></p>
            <strong>Compliance with children's online privacy protection act</strong>
            <p>Protecting the privacy of the very young is especially important. For that reason, we never collect or maintain information at our Site from those we actually know are under 13, and no part of our website is structured to attract anyone under 13.</p>
            <strong>Changes to this privacy policy</strong>
            <p>Writemynamepix has the discretion to update this privacy policy at any time. When we do, we will send you an email. We encourage Users to frequently check this page for any changes to stay informed about how we are helping to protect the personal information we collect. You acknowledge and agree that it is your responsibility to review this privacy policy periodically and become aware of modifications.</p>
            <strong>Your acceptance of these terms</strong>
            <p>By using this Site, you signify your acceptance of this policy. If you do not agree to this policy, please do not use our Site. Your continued use of the Site following the posting of changes to this policy will be deemed your acceptance of those changes.</p>
            <strong>Contacting us</strong>
            <p>If you have any questions about this Privacy Policy, the practices of this site, or your dealings with this site, please contact us at:writemynamepix@gmail.com</p>
        </div>
    </div>
</div>

@stop
