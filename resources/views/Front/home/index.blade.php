@extends($FrontThemeNew)

@section('pageTitle')
<title>{{ $settings['site-title'] }}</title>
@stop

@section('pageContent')        

<h1 class="page-heading">Latest Post</h1>
<p>{!! $settings['site-description'] !!}</p>

<!-- Projects Row -->
<div class="row">
    @if(!empty($latestPost) && $latestPost->count())
        @foreach($latestPost as $key=>$value)
            @include('Front.postData')
        @endforeach
    @endif
</div>
<!-- /.row -->

<hr>

<!-- Pagination -->
{!! $latestPost->render() !!}
<!-- /.row -->
@stop
