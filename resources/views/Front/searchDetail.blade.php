@extends($FrontTheme)

@section('pageTitle')
<title>Your Search : {!! $searchText !!}</title>
@stop

@section('pageContent')
<div class="col-md-9 page-content-right">
    <div class="row headding">
        <div class="col-md-12">
            <hr class="colorgraph">
            <h4>Your Search : "{!! $searchText !!}"</h4>
        </div>
    </div>

    <div class="row pageContent-add">
        <div class="col-md-12">
            {!! $settings['above-post'] !!}
        </div>
    </div>

    <div class="row">
        <div class="col-md-12 col-sm-12">
            <div class="row">
                @if(!empty($searchPostData) && $searchPostData->count())
                    @foreach($searchPostData as $key=>$value)
                        @include('Front.postData')
                    @endforeach
                @endif
            </div>
        </div>
    </div>  

    <div class="row pageContent-add">
        <div class="col-md-12">
            {!! $settings['above-pagination'] !!}
        </div>
    </div>
</div>
@stop
