@if(!empty($displayData) && $displayData->count())
    @foreach($displayData as $key => $value)
        <tr>
            <td>{{ ++$i }}</td>
            <td>{{ $value->name }}</td>
            <td>{{ $value->title }}</td>
            <!-- <td>{{ $value->meta_keyword }}</td> -->
            <td>{{ $value->meta_description }}</td>
            <td>
                <button class="btn btn-primary" data-toggle="modal" data-target="#category-{{ $value->id }}">Edit</button>
                @include('categories.edit')
                <button class="btn btn-danger remove-crud" data-id="{{ $value->id }}" data-action="{{ URL::route('admin.category.remove') }}">Delete</button>
            </td>
        </tr>
    @endforeach
@endif