@extends($theme)

@section('pageTitle')
<title>Categories</title>
@endsection

@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h1 class="page-header">
            Categories Management
            </h1>
        </div>
        <div class="pull-right">
            <button class="btn btn-success" data-toggle="modal" data-target="#create-categories">Create New Category</button>
            <button class="btn btn-primary search-modules">Search</button>
            @include('categories.create')
        </div>
    </div>
</div>

@include('categories.search')

<table class="table table-bordered pagin-table">
    <thead>
        <tr>
            <th>No.</th>
            <th>Name</th>
            <th>Title</th>
            <!-- <th>Keyword</th> -->
            <th>Description</th>
            <th width="220px">Action</th>
        </tr>
    </thead>
    <tbody>
        @include('categories.data')
    </tbody>
</table>

@include('adminTheme.paginate')

<script type="text/javascript">
    $(window).scroll(function(){
        if  ($(window).scrollTop() == $(document).height() - $(window).height()){
           // $.loadPostData();
        }
    });
</script>

@endsection