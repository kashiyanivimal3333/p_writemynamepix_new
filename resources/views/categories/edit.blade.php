<!-- Modal -->
<div class="modal fade" id="category-{{ $value->id }}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            {!! Form::open(array('route' => 'admin.category.edit','autocomplete'=>'off')) !!}
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Category</h4>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="alert alert-danger print-error-msg" style="display:none">
                        <ul></ul>
                    </div>
                    {!! Form::hidden('id', $value->id) !!}
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <label class="form-lable">Name</label>
                                {!! Form::text('name', Input::get('name',$value->name), array('placeholder' => 'Name','class' => 'form-control user-name-edit')) !!}
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6">
                            <div class="form-group">
                                <label class="form-lable">Title</label>
                                {!! Form::text('title', Input::get('title',$value->title), array('placeholder' => 'title','class' => 'form-control')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <label class="form-lable">Meta Keyword</label>
                                {!! Form::textarea('meta_keyword', Input::get('meta_keyword',$value->meta_keyword), array('placeholder' => 'meta keyword','class' => 'form-control user-name-edit','style'=>'height:100px;')) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-12">
                            <div class="form-group">
                                <label class="form-lable">Meta Description</label>
                                {!! Form::textarea('meta_description', Input::get('meta_description',$value->meta_description), array('placeholder' => 'meta description','class' => 'form-control user-name-edit','style'=>'height:100px;')) !!}
                            </div>
                        </div>
                    </div>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary create-crud">Submit</button>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
</div>