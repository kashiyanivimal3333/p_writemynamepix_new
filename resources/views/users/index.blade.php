@extends($theme)

@section('pageTitle')
<title>User</title>
@endsection

@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h1 class="page-header">
            Users Management
            </h1>
        </div>
        <div class="pull-right">
            <button class="btn btn-success" data-toggle="modal" data-target="#create-user">Create New User</button>
            <button class="btn btn-primary search-modules">Search</button>
            @include('users.create')
        </div>
    </div>
</div>

@include('users.search')

<table class="table table-bordered pagin-table">
    <thead>
        <tr>
            <th>No</th>
            <th>Name</th>
            <th>Email</th>
            <th width="220px">Action</th>
        </tr>
    </thead>
    <tbody>
        @include('users.data')
    </tbody>
</table>

@include('adminTheme.paginate')

@endsection