@if(!empty($users) && $users->count())
    @foreach($users as $key => $value)
        <tr>
            <td style="width:30px;">{{ ++$i }}</td>
            <td>{{ $value->name }}</td>
            <td>{{ $value->email }}</td>
            <td>
                <button class="btn btn-primary" data-toggle="modal" data-target="#user-{{ $value->id }}">Edit</button>
                @include('users.edit')
                <button class="btn btn-danger remove-crud" data-id="{{ $value->id }}" data-action="{{ URL::route('admin.users.remove') }}">Delete</button>
                <!-- @if($value->ban == 1)
                    <a href="{{ URL::route('admin.users.revoke',$value->id) }}" class="btn btn-warning">Revoke</a>
                @else
                    <a class="btn btn-success ban" data-id="{{ $value->id }}" data-action="{{ URL::route('admin.users.ban') }}">Ban</a>
                @endif -->
            </td>
        </tr>
    @endforeach
@endif