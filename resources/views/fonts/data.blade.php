@if(!empty($displayData) && $displayData->count())
    @foreach($displayData as $key => $value)
        <tr>
            <td style="width:30px;">{{ ++$i }}</td>
            <td>{{ $value->font_file }}</td>
            <td>
                <button class="btn btn-danger remove-crud" data-id="{{ $value->id }}" data-action="{{ URL::route('admin.font.remove') }}">Delete</button>
            </td>
        </tr>
    @endforeach
@endif