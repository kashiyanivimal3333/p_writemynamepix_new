@extends($theme)

@section('pageTitle')
<title>Fonts</title>
@endsection

@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h1 class="page-header">
            Fonts Management
            </h1>
        </div>
        <div class="pull-right">
            <button class="btn btn-success" data-toggle="modal" data-target="#create-font">Add New Font</button>
            <button class="btn btn-primary search-modules">Search</button>
            @include('fonts.create')
        </div>
    </div>
</div>

@include('fonts.search')

<table class="table table-bordered pagin-table">
    <thead>
        <tr>
            <th style="width:50px;">No.</th>
            <th>Name</th>
            <th width="100px">Action</th>
        </tr>
    </thead>
    <tbody>
        @include('fonts.data')
    </tbody>
</table>

@include('adminTheme.paginate')

<script type="text/javascript">
    $(window).scroll(function(){
        if  ($(window).scrollTop() == $(document).height() - $(window).height()){
           // $.loadPostData();
        }
    });
</script>

@endsection