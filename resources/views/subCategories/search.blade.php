<div class="panel panel-default filter-panel {{Input::get("filter",false)?'filter-in':'filter-out'}}">
    <div class="panel-heading">
        <h3 class="panel-title">Searching</h3>
    </div>
    <div class="panel-body">
        {!! Form::open(array('method'=>'get','class'=>'form-filter form-inline')) !!}
        <ul class="search">
            <li>
                <div class='form-group'>
                    {!! Form::label('filter[name][value]', 'Sub Category',array('class'=>'control-label')) !!}
                    {!! Form::text('filter[name][value]',Input::get('filter.name.value'),array('class'=>'search-crud form-control')) !!}{!! Form::hidden('filter[name][type]','7') !!}
                </div>
            </li>
        </ul>
        {!! Form::close() !!}
    </div>
</div>
