@extends($theme)

@section('pageTitle')
<title>SubCategories</title>
@endsection

@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h1 class="page-header">
            SubCategories Management
            </h1>
        </div>
        <div class="pull-right">
            <button class="btn btn-success" data-toggle="modal" data-target="#create-sub-categories">Create New SubCategory</button>
            <button class="btn btn-primary search-modules">Search</button>
            @include('subCategories.create')
        </div>
    </div>
</div>

@include('subCategories.search')

<table class="table table-bordered pagin-table">
    <thead>
        <tr>
            <th>No.</th>
            <th>Category</th>
            <th>SubCategory</th>
            <th>Title</th>
            <!-- <th>Keyword</th> -->
            <th>Description</th>
            <th width="220px">Action</th>
        </tr>
    </thead>
    <tbody>
        @include('subCategories.data')
    </tbody>
</table>

@include('adminTheme.paginate')

<script type="text/javascript">
    $(window).scroll(function(){
        if  ($(window).scrollTop() == $(document).height() - $(window).height()){
           // $.loadPostData();
        }
    });
</script>

@endsection