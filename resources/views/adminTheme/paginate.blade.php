<div class="ajax-paginate">	
@if(!empty($users) && $users->count())
{!! $users->appends([])->render() !!}
@endif

@if(!empty($displayData) && $displayData->count())
{!! $displayData->appends([])->render() !!}
@endif
</div>