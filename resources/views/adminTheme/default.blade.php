<!DOCTYPE html>
<html lang="en">

<head>
    @yield('pageTitle')
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>{{ $projectTitle }}</title>

    <!-- Bootstrap Core CSS -->
    <link href="{{ asset('/adminTheme/css/bootstrap.min.css') }}" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="{{ asset('/adminTheme/css/sb-admin.css') }}" rel="stylesheet">

    <!-- Morris Charts CSS -->
    <link href="{{ asset('/adminTheme/css/plugins/morris.css') }}" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="{{ asset('/adminTheme/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
    @yield('style')
    <link href="{{ asset('/adminTheme/css/custom.css') }}" rel="stylesheet">

    <!-- jQuery -->
    <script src="{{ asset('/adminTheme/js/jquery.js') }}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('/adminTheme/js/bootstrap.min.js') }}"></script>

    <!-- Morris Charts JavaScript -->
    <script src="{{ asset('/adminTheme/js/plugins/morris/raphael.min.js') }}"></script>

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            
            @include('adminTheme.headerNav')
            
            @include('adminTheme.navbar')

        </nav>

        <div id="page-wrapper" style="min-height:700px;">

            <div class="container-fluid">

                @include('adminTheme.alert')
                @yield('content')

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <script type="text/javascript">
        var current_page_url = "<?php echo URL::full(); ?>";
    </script>
    
    <!-- <script src="{{ asset('/adminTheme/js/plugins/morris/morris.min.js') }}"></script> 
    <script src="{{ asset('/adminTheme/js/plugins/morris/morris-data.js') }}"></script>-->
    <script src="{{ asset('/adminTheme/js/bootbox.js') }}"></script>
    <script src="{{ asset('/adminTheme/js/jquery.form.min.js') }}"></script>
    <script src="{{ asset('/adminTheme/js/crud.js') }}"></script>
    <script src="{{ asset('/adminTheme/js/custom.js') }}"></script>

    @yield('script')

</body>

</html>
