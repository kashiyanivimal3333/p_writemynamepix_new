<!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
<div class="collapse navbar-collapse navbar-ex1-collapse">
    <?php 
        $currentPageURL = URL::current(); 
        $pageArray = explode('/', $currentPageURL);
    ?>
    <ul class="nav navbar-nav side-nav">
        <li class="{{ $pageArray['4'] == 'home' ? 'active' : '' }}">
            <a href="{{ URL::route('admin.home') }}"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
        </li>
        <li class="{{ $pageArray['4'] == 'users' ? 'active' : '' }}">
            <a href="{{ URL::route('admin.users') }}"><i class="fa fa-fw fa-users"></i> User Management</a>
        </li>
        <li class="{{ $pageArray['4'] == 'categories' ? 'active' : '' }}">
            <a href="{{ URL::route('admin.categories') }}"><i class="fa fa-fw fa-list"></i> Categories</a>
        </li>
        <li class="{{ $pageArray['4'] == 'subCategories' ? 'active' : '' }}">
            <a href="{{ URL::route('admin.subCategories') }}"><i class="fa fa-fw fa-indent"></i> SubCategories</a>
        </li>
        <li class="{{ $pageArray['4'] == 'post' ? 'active' : '' }}">
            <a href="{{ URL::route('admin.post') }}"><i class="fa fa-fw fa-newspaper-o"></i> Post</a>
        </li>
        <li class="{{ $pageArray['4'] == 'slider' ? 'active' : '' }}">
            <a href="{{ URL::route('admin.slider') }}"><i class="fa fa-fw fa-sliders"></i> Slider</a>
        </li>
        <li class="{{ $pageArray['4'] == 'fonts' ? 'active' : '' }}">
            <a href="{{ URL::route('admin.fonts') }}"><i class="fa fa-fw fa-font"></i> Fonts</a>
        </li>
        <li class="{{ $pageArray['4'] == 'feedback' ? 'active' : '' }}">
            <a href="{{ URL::route('admin.feedback') }}"><i class="fa fa-fw fa-comment"></i> Feedback</a>
        </li>
        <li class="{{ $pageArray['4'] == 'names' ? 'active' : '' }}">
            <a href="{{ URL::route('admin.names') }}"><i class="fa fa-th"></i> Names</a>
        </li>
    </ul>
</div>
<!-- /.navbar-collapse -->