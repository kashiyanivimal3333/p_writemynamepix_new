@extends($theme)

@section('pageTitle')
<title>Site Settings</title>
@endsection

@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h1 class="page-header">
            Site Settings Management
            </h1>
        </div>
    </div>
</div>

{!! Form::open(array('route' => 'admin.settings.update','autocomplete'=>'off','files'=>'true')) !!}
<div class="grid-body no-border"> <br>
    <div class="row">
        <div class="col-md-8 col-sm-8 col-xs-8">
            <div class="form-group {{ $errors->first($settings['site-title']['slug'], 'has-error') }}">
                <label class="form-label">{!! $settings['site-title']['name'] !!}</label>
                <div class="controls">
                    {!! Form::text($settings['site-title']['slug'], $settings['site-title']['value'], array('placeholder' => 'Site URL','class' => 'form-control user-name-edit')) !!}
                    {!! $errors->first($settings['site-title']['slug'], '<span class="error">:message</span>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->first($settings['site-logo']['slug'], 'has-error') }}">
                <label class="form-label">{!! $settings['site-logo']['name'] !!}</label>
                <div class="controls">
                    {!! Form::file($settings['site-logo']['slug'], null, array('class' => 'form-control')) !!}
                    {!! $errors->first($settings['site-logo']['slug'], '<span class="error">:message</span>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->first($settings['site-favicon']['slug'], 'has-error') }}">
                <label class="form-label">{!! $settings['site-favicon']['name'] !!}</label>
                <div class="controls">
                    {!! Form::file($settings['site-favicon']['slug'], null, array('class' => 'form-control')) !!}
                    {!! $errors->first($settings['site-favicon']['slug'], '<span class="error">:message</span>') !!}
                </div>
            </div>
            
            <div class="form-group {{ $errors->first($settings['site-keyword']['slug'], 'has-error') }}">
                <label class="form-label">{!! $settings['site-keyword']['name'] !!}</label>
                <div class="controls">
                    {!! Form::textarea($settings['site-keyword']['slug'], $settings['site-keyword']['value'], array('placeholder' => 'google analytics code','class' => 'form-control user-name-edit','style'=>'height:100px;')) !!}
                    {!! $errors->first($settings['site-keyword']['slug'], '<span class="error">:message</span>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->first($settings['site-description']['slug'], 'has-error') }}">
                <label class="form-label">{!! $settings['site-description']['name'] !!}</label>
                <div class="controls">
                    {!! Form::textarea($settings['site-description']['slug'], $settings['site-description']['value'], array('placeholder' => 'google analytics code','class' => 'form-control user-name-edit','style'=>'height:100px;')) !!}
                    {!! $errors->first($settings['site-description']['slug'], '<span class="error">:message</span>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->first($settings['address1']['slug'], 'has-error') }}">
                <label class="form-label">{!! $settings['address1']['name'] !!}</label>
                <div class="controls">
                    {!! Form::textarea($settings['address1']['slug'], $settings['address1']['value'], array('placeholder' => 'google analytics code','class' => 'form-control user-name-edit','style'=>'height:100px;')) !!}
                    {!! $errors->first($settings['address1']['slug'], '<span class="error">:message</span>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->first($settings['address2']['slug'], 'has-error') }}">
                <label class="form-label">{!! $settings['address2']['name'] !!}</label>
                <div class="controls">
                    {!! Form::textarea($settings['address2']['slug'], $settings['address2']['value'], array('placeholder' => 'google analytics code','class' => 'form-control user-name-edit','style'=>'height:100px;')) !!}
                    {!! $errors->first($settings['address2']['slug'], '<span class="error">:message</span>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->first($settings['city']['slug'], 'has-error') }}">
                <label class="form-label">{!! $settings['city']['name'] !!}</label>
                <div class="controls">
                    {!! Form::text($settings['city']['slug'], $settings['city']['value'], array('placeholder' => 'Site URL','class' => 'form-control user-name-edit')) !!}
                    {!! $errors->first($settings['city']['slug'], '<span class="error">:message</span>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->first($settings['phone-number']['slug'], 'has-error') }}">
                <label class="form-label">{!! $settings['phone-number']['name'] !!}</label>
                <div class="controls">
                    {!! Form::text($settings['phone-number']['slug'], $settings['phone-number']['value'], array('placeholder' => 'Site URL','class' => 'form-control user-name-edit')) !!}
                    {!! $errors->first($settings['phone-number']['slug'], '<span class="error">:message</span>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->first($settings['email']['slug'], 'has-error') }}">
                <label class="form-label">{!! $settings['email']['name'] !!}</label>
                <div class="controls">
                    {!! Form::text($settings['email']['slug'], $settings['email']['value'], array('placeholder' => 'Site URL','class' => 'form-control user-name-edit')) !!}
                    {!! $errors->first($settings['email']['slug'], '<span class="error">:message</span>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->first($settings['footer-text']['slug'], 'has-error') }}">
                <label class="form-label">{!! $settings['footer-text']['name'] !!}</label>
                <div class="controls">
                    {!! Form::text($settings['footer-text']['slug'], $settings['footer-text']['value'], array('placeholder' => 'Site URL','class' => 'form-control user-name-edit')) !!}
                    {!! $errors->first($settings['footer-text']['slug'], '<span class="error">:message</span>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->first($settings['aboutUs']['slug'], 'has-error') }}">
                <label class="form-label">{!! $settings['aboutUs']['name'] !!}</label>
                <div class="controls">
                    {!! Form::textarea($settings['aboutUs']['slug'], $settings['aboutUs']['value'], array('placeholder' => 'google analytics code','class' => 'form-control user-name-edit ckeditor','style'=>'height:100px;')) !!}
                    {!! $errors->first($settings['aboutUs']['slug'], '<span class="error">:message</span>') !!}
                </div>
            </div>

            <div class="form-group {{ $errors->first($settings['address2']['slug'], 'has-error') }}">
                <label class="form-label">{!! $settings['script']['name'] !!}</label>
                <div class="controls">
                    {!! Form::textarea($settings['script']['slug'], $settings['script']['value'], array('placeholder' => '','class' => 'form-control user-name-edit','style'=>'height:200px;')) !!}
                    {!! $errors->first($settings['script']['slug'], '<span class="error">:message</span>') !!}
                </div>
            </div>

            <div class="form-group">
                <label class="form-label">{!! $settings['above-post']['name'] !!}</label>
                <div class="controls">
                    {!! Form::textarea($settings['above-post']['slug'], $settings['above-post']['value'], array('placeholder' => '','class' => 'form-control user-name-edit','style'=>'height:200px;')) !!}
                </div>
            </div>
            <div class="form-group">
                <label class="form-label">{!! $settings['left-sidebar']['name'] !!}</label>
                <div class="controls">
                    {!! Form::textarea($settings['left-sidebar']['slug'], $settings['left-sidebar']['value'], array('placeholder' => '','class' => 'form-control user-name-edit','style'=>'height:200px;')) !!}
                </div>
            </div>
            <div class="form-group">
                <label class="form-label">{!! $settings['above-pagination']['name'] !!}</label>
                <div class="controls">
                    {!! Form::textarea($settings['above-pagination']['slug'], $settings['above-pagination']['value'], array('placeholder' => '','class' => 'form-control user-name-edit','style'=>'height:200px;')) !!}
                </div>
            </div>
            <div class="form-group">
                <label class="form-label">{!! $settings['above-postimage']['name'] !!}</label>
                <div class="controls">
                    {!! Form::textarea($settings['above-postimage']['slug'], $settings['above-postimage']['value'], array('placeholder' => '','class' => 'form-control user-name-edit','style'=>'height:200px;')) !!}
                </div>
            </div>
            <div class="form-group">
                <label class="form-label">{!! $settings['after-textbox']['name'] !!}</label>
                <div class="controls">
                    {!! Form::textarea($settings['after-textbox']['slug'], $settings['after-textbox']['value'], array('placeholder' => '','class' => 'form-control user-name-edit','style'=>'height:200px;')) !!}
                </div>
            </div>

        </div>
    </div>
    <a href="{{ URL::route('admin.users') }}"><button type="button" class="btn btn-default">Close</button></a>
    <button type="submit" value="general" class="btn btn-primary" name="general">Submit</button>
</div>
{!! Form::close() !!}

<script type="text/javascript">
    $(window).scroll(function(){
        if  ($(window).scrollTop() == $(document).height() - $(window).height()){
           // $.loadPostData();
        }
    });
</script>

@endsection

@section('script')
<script src="{{ asset('/adminTheme/ckeditor/ckeditor.js') }}"></script>
@endsection