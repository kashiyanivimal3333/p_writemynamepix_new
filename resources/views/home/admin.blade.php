@extends($theme)

@section('pageTitle')
<title>Dashboard</title>
@endsection

@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12 margin-tb">
        <h1 class="page-header">
        Dashboard <small>Statistics Overview</small>
        </h1>
    </div>
</div>

<div class="row">
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-indent fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{!! $totalSubCategory !!}</div>
                        <div>SubCategory</div>
                    </div>
                </div>
            </div>
            <a href="#">
            <div class="panel-footer">
                <span class="pull-left">View Details</span>
                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                <div class="clearfix"></div>
            </div>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-green">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-newspaper-o fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{!! $totalPost !!}</div>
                        <div>Post</div>
                    </div>
                </div>
            </div>
            <a href="#">
            <div class="panel-footer">
                <span class="pull-left">View Details</span>
                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                <div class="clearfix"></div>
            </div>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-yellow">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-sliders fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{!! $totalSLider !!}</div>
                        <div>Slider</div>
                    </div>
                </div>
            </div>
            <a href="#">
            <div class="panel-footer">
                <span class="pull-left">View Details</span>
                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                <div class="clearfix"></div>
            </div>
            </a>
        </div>
    </div>
    <div class="col-lg-3 col-md-6">
        <div class="panel panel-red">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <i class="fa fa-comment fa-5x"></i>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div class="huge">{!! $totalFeedback !!}</div>
                        <div>Feedback</div>
                    </div>
                </div>
            </div>
            <a href="#">
            <div class="panel-footer">
                <span class="pull-left">View Details</span>
                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                <div class="clearfix"></div>
            </div>
            </a>
        </div>
    </div>
</div>
<!-- /.row -->
<div class="row">
    <div class="col-lg-6 margin-tb text-center">
        <input name="_token" type="hidden" value="{!! csrf_token() !!}">
        <p>You have dummy images created total : <strong>{{ $countImages }}</strong></p>
        <button data-action="{{ URL::route('admin.removeImages') }}" class="remove-images btn btn-success btn-lg">Delete Images</button>
    </div>

    <!-- <div class="col-lg-6 margin-tb text-center">
        <p><strong>You have Generate Names Images.</strong></p>
        <a href="{{ URL::route('admin.generateNameImages') }}"><button class="btn btn-success btn-lg">Generate Names Images</button></a>
    </div> -->
</div>
@endsection