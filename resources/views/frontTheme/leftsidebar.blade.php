<div class="row">
	<div class="col-md-12 page-content-left">
	@if(!is_null($categoryNew))
	<a href="{!! URL::route('front.categoryDetail',$categoryNew->slug); !!}"><span aria-hidden="true" class="glyphicon glyphicon-hand-right"></span>{!! ucfirst($categoryNew->new_name) !!} <img src="/images/new.gif"></a>
	@endif
	@if(!empty($categoryList))
	    @foreach($categoryList as $key=>$value)
	    	@if(!empty($value['subCatData']))
		        @foreach($value['subCatData'] as $keys=>$values)
		            <a href="{!! URL::route('front.categoryDetail',$values['subCategorySlug']); !!}"><span aria-hidden="true" class="glyphicon glyphicon-hand-right"></span>{!! ucfirst($values['subCategoryName']) !!}</a>
		        @endforeach
			@endif
	    @endforeach
	@endif
	</div>
</div>

<div class="row">
	<div class="col-md-12 col-sm-12 leftSide-add">
	    {!! $settings['left-sidebar'] !!}
	</div>
</div>
