<!DOCTYPE html>
<html lang="en">
    <head>
        @yield('pageTitle')
        <link rel="icon" type="image/png" href="/uploadImages/settings/{!! $settingsData['site-favicon'] !!}">
        <meta name="csrf-token" content="{!! csrf_token() !!}">
        <meta property="fb:app_id" content="1621267068140903" />
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="google-site-verification" content="mLZoe1oK__u3RqL-K7r3muffmIClXtB3YU-wSxM6ggo" />
        
        {!! SEOMeta::generate() !!}
        {!! OpenGraph::generate() !!}
        {!! Twitter::generate() !!}
        
        @include('frontTheme.style')
        
        @yield('PageLevelStyle')

        @include('frontTheme.script')
        
    </head>
    <body>
        @include('frontTheme.header')

        @include('frontTheme.menu')
        
        <div class="page-content">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        @include('frontTheme.leftsidebar')
                    </div>
                    @yield('pageContent')
                </div>
            </div>    
        </div>

        @include('frontTheme.footer')
            
        @yield('PageLevelScript')
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.lazyload/1.9.1/jquery.lazyload.js"></script>

        <script type="text/javascript">
            $("img").lazyload({
                effect : "fadeIn"
            });
        </script>

        <script type="text/javascript">
            $(function() {
                var pull = $('#mobile-menu');
                menu = $('#menu nav');
                menuHeight = menu.height();

                $(pull).on('click', function(e) {
                    e.preventDefault();
                    menu.slideToggle("slow");
                });

                $(window).resize(function() {
                    var w = $(window).width();
                    if (w > 320 && menu.is(':hidden')) {
                        menu.removeAttr('style');
                    }
                });
            });
            
            $(document).ready(function(){
                $('body').append('<div id="toTop" class="btn btn-info"><span class="glyphicon glyphicon-chevron-up"></span></div>');
                $(window).scroll(function () {
                    if ($(this).scrollTop() != 0) {
                        $('#toTop').fadeIn();
                    } else {
                        $('#toTop').fadeOut();
                    }
                }); 
                $('#toTop').click(function(){
                    $("html, body").animate({ scrollTop: 0 }, 600);
                    return false;
                });
            });

        </script>

        {!! $settings['script'] !!}
    </body>
</html>