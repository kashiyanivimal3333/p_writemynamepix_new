<?php
    $currentPageURL = URL::current(); 
    $pageArray = explode('/', $currentPageURL);
    $pageActive = isset($pageArray[3]) ? $pageArray[3] : 'home';
?>
<div class="menu">
    <div class="container menu-sub">
        <div class="row">
            <div class="col-md-3 categorys">
                <h2 class="pull-left">Categories</h2>
                <span aria-hidden="true" class="glyphicon glyphicon-chevron-down pull-right"></span>
            </div>
            <div class="mobile-menu" id="mobile-menu">Menu <i class="fa fa-bars pull-right" style="font-size:20px;"></i></div>
            <div class="col-md-9 main-menu" id="menu">
                <nav>
                    <ul>
                        <li><a href="{{ URL::route('front.home') }}" class="{{ $pageActive == 'home' ? 'active' : ''  }}">Home</a></li>
                        <li><a href="{{ URL::route('front.howtowrite') }}" class="{{ $pageActive == 'howtowrite.html' ? 'active' : ''  }}">How to write name</a></li>
                        <li><a href="{{ URL::route('front.policy') }}" class="{{ $pageActive == 'policy.html' ? 'active' : ''  }}">Privacy Policy</a></li>
                        <li><a href="{{ URL::route('front.about') }}" class="{{ $pageActive == 'about.html' ? 'active' : ''  }}">About US</a></li>
                        <li><a href="{{ URL::route('front.contact') }}" class="{{ $pageActive == 'contact.html' ? 'active' : ''  }}">Contact Us</a></li>
                    </ul>
                </nav>
            </div>
        </div>
    </div>    
</div>