<div class="footer">
    <div class="container">
        <div class="col-md-3">
            <h4>Menu</h4>
            <ul class="footer-menu">
                <li><a href="{{ URL::route('front.home') }}"> <span aria-hidden="true" class="glyphicon glyphicon-hand-right"></span> Home</a></li>
                <li><a href="{{ URL::route('front.howtowrite') }}"> <span aria-hidden="true" class="glyphicon glyphicon-hand-right"></span> How to write name</a></li>
                <li><a href="{{ URL::route('front.policy') }}"> <span aria-hidden="true" class="glyphicon glyphicon-hand-right"></span> Privacy Policy</a></li>
                <li><a href="{{ URL::route('front.about') }}"> <span aria-hidden="true" class="glyphicon glyphicon-hand-right"></span> About US</a></li>
                <li><a href="{{ URL::route('front.contact') }}"> <span aria-hidden="true" class="glyphicon glyphicon-hand-right"></span> Contact Us</a></li>
            </ul>
        </div>
        <div class="col-md-5 footer-category-main">
            <h4>Category</h4>
            <ul class="footer-category">
                @if(!empty($categoryList))
                    @foreach($categoryList as $key=>$value)
                        @if(!empty($value['subCatData']))
                        @foreach($value['subCatData'] as $keys=>$values)
                            <li><a href="{!! URL::route('front.categoryDetail',$values['subCategorySlug']); !!}"> <span aria-hidden="true" class="glyphicon glyphicon-hand-right"></span> {!! $values['subCategoryName'] !!} </a></li>
                        @endforeach
                        @endif
                    @endforeach
                @endif
            </ul>
        </div>
        <div class="col-md-4 footer-sosial">
            <h4>Social</h4>
            <div class="row">
                <div class="col-md-4 col-md-offset-2 footer-sosial-mobi">
                    <a href="https://www.facebook.com/writemynamepix"><img src="/images/face book.png"></a>
                </div>
                <div class="col-md-4 footer-sosial-mobi">
                    <a href="https://plus.google.com/103933247688195546514"><img src="/images/g+.png"></a>
                </div>
            </div><br>
            <div class="row">
                <div class="col-md-4 col-md-offset-2 footer-sosial-mobi">
                    <a href="https://twitter.com/hdwwall123"><img src="/images/twitter.png"></a>
                </div>
                <div class="col-md-4 footer-sosial-mobi">
                    <a href="#"><img src="/images/linked in.png"></a>
                </div>
            </div>
        </div>
    </div>
    <div class="footer-bottom">
        <p>{!! $settings['footer-text'] !!}</p>
    </div>
</div>