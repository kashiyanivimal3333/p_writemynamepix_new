<!-- Bootstrap Core CSS -->
<link href="{{ asset('/frontTheme/css/bootstrap.min.css') }}" rel="stylesheet">
<!-- Custom Fonts -->
<link href="{{ asset('/frontTheme/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css">
<link href="{{ asset('/frontTheme/css/font.css') }}" rel="stylesheet">
<link href="{{ asset('/frontTheme/css/custom.css') }}" rel="stylesheet">
<link href="{{ asset('/frontTheme/css/mobile-custom.css') }}" rel="stylesheet">