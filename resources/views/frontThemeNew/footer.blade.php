<footer>
    <div class="row">
        <div class="col-lg-12">
            <p>{!! $settings['footer-text'] !!}</p>
        </div>
    </div>
    <!-- /.row -->
</footer>