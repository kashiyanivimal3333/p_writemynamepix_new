<h3 class="cat-title">Categories</h3>
<ul class="list-group">
  	@if(!empty($categoryList))
	    @foreach($categoryList as $key=>$value)
	    	@if(!empty($value['subCatData']))
		        @foreach($value['subCatData'] as $keys=>$values)
		            <a href="{!! URL::route('front.categoryDetail',$values['subCategorySlug']); !!}"><li class="list-group-item">{!! ucfirst($values['subCategoryName']) !!}</li></a>
		        @endforeach
			@endif
	    @endforeach
	@endif
</ul>

<div class="row">
	<div class="col-md-12 col-sm-12 leftSide-add">
	    {{-- {!! $settings['left-sidebar'] !!} --}} 
	</div>
</div>
