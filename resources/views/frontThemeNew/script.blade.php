<!-- jQuery -->
<script src="{{ asset('/frontTheme/js/jquery.js') }}"></script>
<!-- Bootstrap Core JavaScript -->
<script src="{{ asset('/frontTheme/js/bootstrap.min.js') }}"></script>
@yield('PageLevelScript')
