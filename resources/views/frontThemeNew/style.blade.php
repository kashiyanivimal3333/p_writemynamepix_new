<!-- Bootstrap Core CSS -->
<link href="{{ asset('/frontTheme/css/bootstrap.min.css') }}" rel="stylesheet">
<!-- Custom CSS -->
<link href="{{ asset('/frontTheme/css/2-col-portfolio.css') }}" rel="stylesheet">
<link href="{{ asset('/frontTheme/css/custom.css') }}" rel="stylesheet">
@yield('PageLevelStyle')