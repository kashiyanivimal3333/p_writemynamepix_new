<div class="header">
    <div class="container header-sub">
        <div class="row">
            <div class="col-md-4 logo">
                <a href="{{ URL::route('front.home') }}">
                    <h2>WRITE MY NAME PIX</h2>
                    <span>Welcome To writemynamepix.com</span>
                </a>
            </div>
            <div class="col-md-8 search-header">
                {!! Form::open(array('route' => 'front.search.get','method'=>'post','autocomplete'=>'off','class'=>'form-horizontal')) !!}
                    <div class="input-group col-md-6 pull-right search-text">
                        {!! Form::text('search',Input::get('search'),array('class'=>'form-control input-search','placeholder'=>'Enter Your KeyWord Like - Birthday, Special Days','required'=>'required')) !!}
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-default btn-search"><i class="glyphicon glyphicon-search"></i></button>
                        </span>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>    
</div>