<!DOCTYPE html>
<html lang="en">

<head>

    @yield('pageTitle')
    <link rel="icon" type="image/png" href="/uploadImages/settings/{!! $settings['site-favicon'] !!}">
    <meta name="csrf-token" content="{!! csrf_token() !!}">
    <meta property="fb:app_id" content="1621267068140903" />
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="google-site-verification" content="mLZoe1oK__u3RqL-K7r3muffmIClXtB3YU-wSxM6ggo" />
    
    {!! SEOMeta::generate() !!}
    {!! OpenGraph::generate() !!}
    {!! Twitter::generate() !!}

    @include('frontThemeNew.script')
    @include('frontThemeNew.style')

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <!-- Navigation -->
    @include('frontThemeNew.menu')

    <!-- Page Content -->
    <div class="container">

        <div class="row">
            <div class="col-md-9">
                @yield('pageContent')
            </div>
            <div class="col-md-3">
                @include('frontThemeNew.rightsidebar')
            </div>
        </div>

        <hr>

        <!-- Footer -->
        @include('frontThemeNew.footer')

    </div>
    <!-- /.container -->

    {!! $settings['script'] !!}
</body>

</html>
