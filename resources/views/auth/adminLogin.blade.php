@extends($login)
@section('content')
<div class="row vertical-offset-100">
    <div class="col-md-4 col-md-offset-4">
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="row-fluid user-row">
                    <img src="/adminTheme/images/logo_sm_2_mr_1.png" class="img-responsive" alt="Conxole Admin"/>
                </div>
            </div>
            <div class="panel-body">
                {!! Form::open(array('route' => 'admin.login.post','class'=>'form-signin')) !!}
                    <fieldset>
                        <label class="panel-login">
                            <div class="login_result">
                            	@if (count($errors) > 0)
									<div class="alert alert-danger" style="width:100%">
										<ul>
											@foreach ($errors->all() as $error)
												<li>{{ $error }}</li>
											@endforeach
										</ul>
									</div>
								@endif
                            </div>
                        </label>
                        {!! Form::text('email', Input::get('email'), array('placeholder' => 'Email','class' => 'form-control')) !!}
                        {!! Form::password('password', array('placeholder' => 'Password','class'=>'form-control')) !!}
                        <br></br>
                        <input class="btn btn-lg btn-success btn-block" type="submit" id="login" value="Login »">
                    </fieldset>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
</div>
@endsection