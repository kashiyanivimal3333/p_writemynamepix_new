@if(!empty($displayData) && $displayData->count())
    @foreach($displayData as $key => $value)
        <tr>
            <td style="width:30px;">{{ ++$i }}</td>
            <td style="text-align:center;"><img src="/uploadImages/slider/{!! $value->image !!}" style="height:100px;width:200px;"></td>
            <td style="width:50px;">
                <button class="btn btn-danger remove-crud" data-id="{{ $value->id }}" data-action="{{ URL::route('admin.slider.remove') }}">Delete</button>
            </td>
        </tr>
    @endforeach
@endif