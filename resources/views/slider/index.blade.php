@extends($theme)

@section('pageTitle')
<title>Slider</title>
@endsection

@section('content')
<!-- Page Heading -->
<div class="row">
    <div class="col-lg-12 margin-tb">
        <div class="pull-left">
            <h1 class="page-header">
            Slider Management
            </h1>
        </div>
        <div class="pull-right">
            <button class="btn btn-success" data-toggle="modal" data-target="#create-slider">Add New Image</button>
            @include('slider.create')
        </div>
    </div>
</div>

<table class="table table-bordered pagin-table">
    <thead>
        <tr>
            <th style="width:50px;">No.</th>
            <th style="text-align;">Image</th>
            <th width="220px">Action</th>
        </tr>
    </thead>
    <tbody>
        @include('slider.data')
    </tbody>
</table>

@include('adminTheme.paginate')

<script type="text/javascript">
    $(window).scroll(function(){
        if  ($(window).scrollTop() == $(document).height() - $(window).height()){
           // $.loadPostData();
        }
    });
</script>

@endsection