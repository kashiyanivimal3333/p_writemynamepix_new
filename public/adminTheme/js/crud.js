var pageCount = 1;
var formSerial = "";

var optionsUser = { 
    complete: function(response) 
    {
    	if($.isEmptyObject(response.responseJSON.error)){
    		getPosts(pageCount,formSerial);
    		$(".modal").modal('hide');
    		$(".modal-backdrop").remove();
    		$("body").removeClass("modal-open");
    	}else{
    		printErrorMsg(response.responseJSON.error);
    	}
    }
};

$("body").on("click",".create-crud",function(){
	$(this).parents("form").ajaxForm(optionsUser);
});

function printErrorMsg (msg) {
	$(".print-error-msg").find("ul").html('');
	$(".print-error-msg").css('display','block');
	$.each( msg, function( key, value ) {
		$(".print-error-msg").find("ul").append('<li>'+value+'</li>');
	});
}

$("body").on("click",".remove-crud",function(){
	var current_object = $(this);
	bootbox.confirm("Are you sure delete this item?", function(result) {
		if(result){	
		  	var action = current_object.attr('data-action');
		  	var token = $("input[name='_token']").val();
			var id = current_object.attr('data-id');
			$('body').html("<form class='form-inline remove-form' method='POST' action='"+action+"'></form>");
			$('body').find('.remove-form').append('<input name="_token" type="hidden" value="'+ token +'">');
			$('body').find('.remove-form').append('<input name="id" type="hidden" value="'+ id +'">');
			$('body').find('.remove-form').submit();
		}
	}); 
});

$("body").on("click",".remove-images",function(){
    var current_object = $(this);
    bootbox.confirm("Are you sure to remove all images?", function(result) {
        if(result){ 
            var action = current_object.attr('data-action');
            var token = $("input[name='_token']").val();
            $('body').html("<form class='form-inline remove-form' method='POST' action='"+action+"'></form>");
            $('body').find('.remove-form').append('<input name="_token" type="hidden" value="'+ token +'">');
            $('body').find('.remove-form').submit();
        }
    }); 
});

$("body").on("click",".ban",function(){

	  var current_object = $(this);

	  bootbox.dialog({
	  message: "<form class='form-inline add-to-ban' method='POST'><div class='form-group'><textarea class='form-control reason' rows='4' style='width:500px' placeholder='Add Reason for Ban this user.'></textarea></div></form>",
	  title: "Add To Black List",
	  buttons: {
	    success: {
	      label: "Submit",
	      className: "btn-success",
	      callback: function() {
	      		var baninfo = $('.reason').val();
	      		var token = $("input[name='_token']").val();
	      		var action = current_object.attr('data-action');
	      		var id = current_object.attr('data-id');

	        	if(baninfo == ''){
	        		$('.reason').css('border-color','red');
	        		return false;
	        	}else{
	        		$('.add-to-ban').attr('action',action);
	        		$('.add-to-ban').append('<input name="_token" type="hidden" value="'+ token +'">')
	        		$('.add-to-ban').append('<input name="id" type="hidden" value="'+ id +'">')
	        		$('.add-to-ban').append('<input name="baninfo" type="hidden" value="'+ baninfo +'">')
	        		$('.add-to-ban').submit();
	        	}
	        	
	      }
	    },
	    danger: {
	      label: "Cancel",
	      className: "btn-danger",
	      callback: function() {
	      	// remove
	      }
	    },
	  }
	});
});

/*=====  End of Add to Link Ajax  ======*/
    
/*======================================
=            Load Post Data            =
======================================*/
var postLoadPage = 1;
var flagPostLoad = 0;

$.loadPostData = function() {
    var token = $('meta[name=csrf-token]').attr("content");
    postLoadPage++;
    var form = $(".form-filter").serialize();
    form = form + '&page=' + postLoadPage;
    console.log(form);

    if(flagPostLoad == 0){
        $.ajax({
            url: current_page_url,
            method: 'GET',
            data: form,
            success: function(data) {
                if(data != ""){
                    $(".pagin-table").find("tbody").append(data);
                }else{
                    flagPostLoad = 1;
                }
            }
        });
    }
}

$(".search-crud").keyup(function(){
	formSerial = $(this).parents("form").serialize();
	getPosts(1,formSerial);
});

jQuery(".search-modules").click(function(e) {
    e.preventDefault();
    jQuery(".filter-panel").toggle("slow");
});

$(window).on('hashchange', function() {
    if (window.location.hash) {
        var page = window.location.hash.replace('#', '');
        if (page == Number.NaN || page <= 0) {
            return false;
        } else {
            // getPosts(page,formSerial);
        }
    }
});

$(document).ready(function() {
    $(document).on('click', '.pagination > li > a', function (e) {
    	formSerial = $(".search-crud").parents("form").serialize();
        getPosts($(this).attr('href').split('page=')[1],formSerial);
        e.preventDefault();
    });
});

function getPosts(page, filter) {
	formSerial = filter;
	pageCount = page;
    $.ajax({
        url : '?page=' + page + '&' + filter,
        dataType: 'json',
    }).done(function (data) {
        $('.pagin-table').find("tbody").html(data.data);
        $('.ajax-paginate').html(data.paginate);
        // for color picker
        $(function(){
            $('.demo2').colorpicker();
    	});
        // for Image load
        $(function() {
          $('.morefiles').on('change', gotPic);
        });
        CKEDITOR.replace( 'editor1' );
        location.hash = page;
    }).fail(function () {
        alert('Posts could not be loaded.');
    });
}